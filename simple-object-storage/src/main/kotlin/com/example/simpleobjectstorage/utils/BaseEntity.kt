package com.example.simpleobjectstorage.utils

import org.springframework.data.annotation.CreatedBy
import org.springframework.data.annotation.CreatedDate
import org.springframework.data.annotation.LastModifiedBy
import org.springframework.data.annotation.LastModifiedDate
import org.springframework.data.jpa.domain.support.AuditingEntityListener
import java.io.Serializable
import java.time.Instant
import java.util.*
import javax.persistence.*


@EntityListeners(AuditingEntityListener::class)
@MappedSuperclass
abstract class BaseEntity : Serializable {
    @Temporal(TemporalType.TIMESTAMP)
    @CreatedDate
    @Column(updatable = false)
    var createdDate: Date? = null

    @CreatedBy
    @Column(updatable = false)
    var createdBy: String? = null

    @Temporal(TemporalType.TIMESTAMP)
    @LastModifiedDate
    var modifiedDate: Date? = null

    @LastModifiedBy
    var modifiedBy: String? = null

    var active : Boolean = true

    @Version
    @Column(name = "optlock", columnDefinition = "integer DEFAULT 0", nullable = false)
    private val version: Long? = null
}