package com.example.simpleobjectstorage.utils

import org.springframework.validation.FieldError
import java.util.stream.Collectors

class UniversalResponseBody() {

//    var info: String? = null
//
//    var status: String? = null
//
//    constructor(status: String, info: String) : this(){
//        this.info = info
//        this.status = status
//    }


    var fieldErrors: MutableList<FieldError> = ArrayList()
    var data : MutableMap<String, Any> = HashMap()

    fun addFieldError(field:String, code:String, message:String) : UniversalResponseBody {
        fieldErrors.add(FieldError(field, code, message))
        return this
    }

    fun put(key: String, value: Any?) : UniversalResponseBody {
        if(value != null)
            data[key] = value
        return this
    }

    fun build() : Map<String, Any> {
        var response : MutableMap<String, Any> = HashMap()
        response.putAll(data)
        var fieldErrorMap = fieldErrors.stream().map { x -> mapFieldError(x) }.collect(Collectors.toList())
        if (!fieldErrorMap.isEmpty()) {
            response["field_errors"] = fieldErrorMap
        }
        return response
    }

    fun mapFieldError(fieldError: FieldError) : MutableMap<String, Any> {
        var errMap: MutableMap<String, Any> = HashMap()
        errMap["field"] = fieldError.field
        errMap["code"] = fieldError.code!!
        if(fieldError.defaultMessage != null) {
            errMap["message"] = fieldError.defaultMessage!!
        }
        return errMap
    }

}