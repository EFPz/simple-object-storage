package com.example.simpleobjectstorage.config

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ContentNegotiationConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;


@Configuration
class ContentNegotiationConfiguration : WebMvcConfigurer {


     override fun configureContentNegotiation(configurer: ContentNegotiationConfigurer) {
        // Turn off suffix-based content negotiation
        configurer.favorPathExtension(false)
    }
}