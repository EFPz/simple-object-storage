package com.example.simpleobjectstorage.config

import org.springframework.data.domain.AuditorAware
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.stereotype.Component
import java.util.*

@Component
class AuditorAwareImpl : AuditorAware<String> {

    override fun getCurrentAuditor(): Optional<String> {

//        var userDetails = SecurityContextHolder.getContext().authentication
//        try {
//            if (userDetails != null && userDetails.isAuthenticated && userDetails.principal != null) {
//                return when {
//                    userDetails.principal is UserDetails -> Optional.ofNullable((userDetails.principal as UserDetails).username)
//                    userDetails.principal is User -> Optional.of((userDetails.principal as User).username!!)
//                    else -> Optional.of(userDetails.principal as String)
//                }
//            }
//        } catch (e: NullPointerException) {
//            return Optional.empty()
//        } catch (e: IllegalStateException) {
//            return Optional.empty()
//        }

        return Optional.empty()
    }
}