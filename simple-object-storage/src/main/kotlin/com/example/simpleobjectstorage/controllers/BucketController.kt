package com.example.simpleobjectstorage.controllers

import com.example.simpleobjectstorage.modules.bucket.Bucket
import com.example.simpleobjectstorage.modules.bucket.BucketRepository
import com.example.simpleobjectstorage.modules.bucket.BucketService
import com.example.simpleobjectstorage.modules.bucket.dto.BucketCreateInfoResponseDto
import com.example.simpleobjectstorage.modules.bucket.dto.BucketListDto
import com.example.simpleobjectstorage.modules.bucket.dto.BucketListInfoResponseDto
import com.example.simpleobjectstorage.utils.UniversalResponseBody
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.validation.BindingResult
import com.sun.xml.internal.ws.spi.db.BindingContextFactory.LOGGER
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.transaction.annotation.Transactional
import org.springframework.web.bind.annotation.*
import java.io.UnsupportedEncodingException
import org.springframework.web.util.UriUtils
import javax.servlet.http.HttpServletRequest

@RestController
@RequestMapping(value = ["{bucketname}"])
@CrossOrigin
class BucketController {

    @Autowired
    lateinit var bucketService : BucketService

    @Autowired
    lateinit var bucketRepository : BucketRepository

    @RequestMapping(
            params = ["create"],
            method = [(RequestMethod.POST)])
    fun createBucket(@PathVariable("bucketname") name: String): ResponseEntity<Any> {
        return try {
            return ResponseEntity.ok(BucketCreateInfoResponseDto(bucketService.create(name)))
        }catch (e: Exception){
            ResponseEntity.badRequest().body(UniversalResponseBody()
                    .put("exception",e.cause!!.message!!)
                    .put("status",400)
                    .build())

        }

    }



    @RequestMapping(
            params = ["delete"],
            method = [(RequestMethod.DELETE)])
    fun deleteBucket(@PathVariable("bucketname") name: String): ResponseEntity<Any> {
        return try {
            bucketService.delete(name)
            ResponseEntity.ok(UniversalResponseBody().put("result","Delete Bucket name $name Successful").build())
        }catch (e: Exception){
            ResponseEntity.badRequest().body(UniversalResponseBody()
                    .put("exception",e.cause!!.message!!)
                    .put("status",400)
                    .build())

        }
    }
    @RequestMapping(
            params = ["list"],
            method = [(RequestMethod.GET)])
    fun listBucket(@PathVariable("bucketname") name: String): ResponseEntity<Any> {
        return try{
            ResponseEntity.ok(BucketListInfoResponseDto(bucketService.list(name)))
        }catch (e: Exception){
            ResponseEntity.badRequest().body(UniversalResponseBody()
                    .put("exception",e.cause!!.message!!)
                    .put("status",400)
                    .build())
        }
    }


    @RequestMapping(
            params = ["all"],
            method = [(RequestMethod.GET)])
    fun listAllBucket(@PathVariable("bucketname") name: String): ResponseEntity<Any> {
        return try{
            ResponseEntity.ok(bucketRepository.findAll())
        }catch (e: Exception){
            ResponseEntity.badRequest().body(UniversalResponseBody()
                    .put("exception",e.cause!!.message!!)
                    .put("status",400)
                    .build())
        }
    }


    @RequestMapping(value = [("/status"), ("/status/")], method = [(RequestMethod.GET)])
    fun status(): String {
        return "Yep"
    }


}