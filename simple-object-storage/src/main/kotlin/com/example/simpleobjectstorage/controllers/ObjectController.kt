package com.example.simpleobjectstorage.controllers

import com.example.simpleobjectstorage.modules.`object`.ObjectStorageRepository
import com.example.simpleobjectstorage.modules.`object`.ObjectStorageService
import com.example.simpleobjectstorage.modules.`object`.dto.ObjectCreateInfoResponseDto
import com.example.simpleobjectstorage.modules.`object`.dto.ObjectUploadInfoResponseDtoFailed
import com.example.simpleobjectstorage.modules.`object`.dto.ObjectUploadInfoResponseDtoSuccess
import com.example.simpleobjectstorage.modules.`object`.metadata.ObjectMetaDataRepository
import com.example.simpleobjectstorage.modules.`object`.metadata.ObjectMetaDataService
import com.example.simpleobjectstorage.modules.part.PartObjectService
import com.example.simpleobjectstorage.modules.part.dto.PartCreateDtoByObjectName
import com.example.simpleobjectstorage.utils.UniversalResponseBody
import org.apache.commons.io.IOUtils
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.security.crypto.codec.Hex
import org.springframework.web.bind.annotation.*
import java.util.regex.Matcher
import java.util.regex.Pattern
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse


@RestController
@RequestMapping(value = ["{bucketname}/{objectname}"])
@CrossOrigin
class ObjectController {

    @Autowired
    lateinit var objectStorageRepository: ObjectStorageRepository

    @Autowired
    lateinit var objectStorageService: ObjectStorageService

    @Autowired
    lateinit var partObjectService: PartObjectService

    @Autowired
    lateinit var objectMetaDataService: ObjectMetaDataService


    @RequestMapping(
            params = ["create"],
            method = [(RequestMethod.POST)])
    fun createObject(@PathVariable("bucketname") bucketName: String,
                     @PathVariable("objectname") objectName: String): ResponseEntity<Any> {
        return try {
            return ResponseEntity.ok(ObjectCreateInfoResponseDto(objectStorageService.create(bucketName,objectName)))
        }catch (e: Exception){
            ResponseEntity.badRequest().body(UniversalResponseBody()
                    .put("exception",e.message)
                    .put("reason","ObjectStorage already exist or cannot be created,")
                    .put("status",400)
                    .build())

        }

    }


    @RequestMapping(
            method = [(RequestMethod.PUT)])
    fun uploadAllParts(@PathVariable("bucketname") bucketName: String,
                       @PathVariable("objectname") objectName: String,
                       @RequestParam("partNumber") partNumber: Int,
                       @RequestHeader("Content-Length") length: Int,
                       @RequestHeader("Content-MD5") md5: String,
                       request: HttpServletRequest): ResponseEntity<Any> {
//        "LengthMismatched|MD5Mismatched|InvalidPartNumber|InvalidObjectName|InvalidBucket"
        return try {

//            Check before write
            objectStorageService.checkTicketValidity(bucketName,objectName,partNumber)
            objectStorageService.checkCompleteStatus(bucketName,objectName)
//            write
            val location = objectStorageService.uploadPart(bucketName,objectName,partNumber,length,md5,request)
//            Check after write
            try{
                objectStorageService.checkHeaderValidity(length,md5,location)
                objectStorageService.checkCompleteStatus(bucketName,objectName)
            }catch (e : Exception){
//                Delete file
                objectStorageService.deleteFile(location)
                throw Exception(e.message)

            }

//            Link obj with part
            val pDto = PartCreateDtoByObjectName()
            pDto.obj = objectName
            pDto.length = length
            pDto.name = objectName
            pDto.partNumber = partNumber
            pDto.complete = true
            pDto.location = location
            pDto.md5 = md5
            pDto.bucketName = bucketName

            partObjectService.createByObjectName(pDto)

            ResponseEntity.ok(ObjectUploadInfoResponseDtoSuccess(md5,length,partNumber))

        }catch (e: Exception){
//            if (e.cause!!.message == "File already completed"){
////               File already marked as complete, so don't do anything
//                ResponseEntity.ok(ObjectUploadInfoResponseDtoSuccess(md5,request.getHeader("Content-Length"),partNumber))
//            }
            if(e.cause == null || e.cause!!.message == null){
                return ResponseEntity.badRequest().body(UniversalResponseBody()
                        .put("message","Internal Error! Content-Length or MD5 might be wrong"))
            }
//            return  ResponseEntity.badRequest().body(e)
            return ResponseEntity.badRequest().body(ObjectUploadInfoResponseDtoFailed(md5,length,partNumber,e.cause!!.message!!))
        }

    }

    @RequestMapping(
            params = ["complete"],
            method = [(RequestMethod.POST)])
    fun complete(@PathVariable("bucketname") bucketName: String,
                     @PathVariable("objectname") objectName: String): ResponseEntity<Any> {
        return try {
            val obj = objectStorageService.complete(bucketName,objectName)
            return ResponseEntity.ok(UniversalResponseBody()
                    .put("eTag",obj.objectETag)
                    .put("length",obj.totalSize)
                    .put("name",obj.name)
                    .build()
            )
        }catch (e: Exception){
            ResponseEntity.badRequest().body(UniversalResponseBody()
                    .put("name",objectName)
                    .put("error",e.cause!!.message)
                    .build())

        }

    }

    @RequestMapping(method = [(RequestMethod.DELETE)])
    fun deleteObject(@PathVariable("bucketname") bucketName: String,
                   @PathVariable("objectname") objectName: String): ResponseEntity<Any> {
        return try {
            val obj = objectStorageService.deleteObject(bucketName,objectName)
            ResponseEntity.ok("")
        }catch (e: Exception){
            ResponseEntity.badRequest().body(UniversalResponseBody()
                    .put("name",objectName)
                    .put("error",e.cause!!.message)
                    .build())

        }

    }


    @RequestMapping(params = [("list")],method = [(RequestMethod.GET)])
    fun list(@PathVariable("bucketname") bucketName: String,
             @PathVariable("objectname") objectName: String): ResponseEntity<Any>{
        return try{
            ResponseEntity.ok(UniversalResponseBody()
                    .put("Object",objectStorageService.getByName(bucketName,objectName)!!)
                    .build())
        }catch (e: Exception){
            ResponseEntity.badRequest().body(UniversalResponseBody()
                    .put("error",e)
                    .put("status",400)
                    .build())
        }
    }


    @RequestMapping(method = [(RequestMethod.GET)])
    fun download(@PathVariable("bucketname") bucketName: String,
                 @PathVariable("objectname") objectName: String,
                 request: HttpServletRequest,
                 response: HttpServletResponse): ResponseEntity<Any>{
        return try{

            var range = request.getHeader("Range")
            if(range == null){
                range ="Bytes=0-"
            }
            var getAllParts = false
            if(range == ""){
                getAllParts = true
                objectStorageService.download(bucketName,objectName,0,0,response,getAllParts)
            }else{
                val pattern = Pattern.compile("(-?[0-9]*)-(-?[0-9]*)")
                val matcher = pattern.matcher(range)
                matcher.find()

                val from = matcher.group(1).toInt()

                var to = 0
                to = if(matcher.group(2) == ""){
                    -1
                }else{
                    matcher.group(2).toInt()
                }

                objectStorageService.download(bucketName,objectName,from,to,response,getAllParts)
            }

            ResponseEntity.ok().build()
        }
        catch (e: Exception){
//            throw e
            ResponseEntity.badRequest().build()
        }
    }




    @RequestMapping(method = [(RequestMethod.PUT)], params = ["metadata"])
    fun updateMetadata(@PathVariable("bucketname") bucketName: String,
                       @PathVariable("objectname") objectName: String,
                       @RequestParam("key") key: String,
                       @RequestBody body: String,
                       request: HttpServletRequest): ResponseEntity<Any> {
        return try {
            objectMetaDataService.updateMetadata(bucketName,objectName,key,body)
            ResponseEntity.ok(UniversalResponseBody().put("status", 200).build())
        } catch(e: Exception) {
            return ResponseEntity.badRequest().body(UniversalResponseBody().put("status", e.cause!!.message).build())
        }

    }

    @RequestMapping(method = [(RequestMethod.DELETE)], params = ["metadata"])
    fun deleteMetadata(@PathVariable("bucketname") bucketName: String,
                       @PathVariable("objectname") objectName: String,
                       @RequestParam("key") key: String,
                       request: HttpServletRequest): ResponseEntity<Any> {
        return try {
            objectMetaDataService.deleteMetadata(bucketName,objectName,key)
            ResponseEntity.ok(UniversalResponseBody().put("status", 200).build())
        } catch(e: Exception) {
            return ResponseEntity.badRequest().body(UniversalResponseBody().put("status", e.cause!!.message).build())
        }

    }

    @RequestMapping(method = [(RequestMethod.GET)], params = ["metadata", "key"])
    fun getMetadataByKey(@PathVariable("bucketname") bucketName: String,
                         @PathVariable("objectname") objectName: String,
                         @RequestParam("key") key: String,
                         request: HttpServletRequest): ResponseEntity<Any> {
        return try {
            ResponseEntity.ok(objectMetaDataService.getMetadataByKey(bucketName,objectName,key))
        } catch(e: Exception) {
            return ResponseEntity.badRequest().body(UniversalResponseBody().put("status", e.cause!!.message).build())
        }

    }

    @RequestMapping(method = [(RequestMethod.GET)], params = ["metadata"])
    fun getAllMetadata(@PathVariable("bucketname") bucketName: String,
                       @PathVariable("objectname") objectName: String,
                       request: HttpServletRequest): ResponseEntity<Any> {
        return try {
            ResponseEntity.ok(objectMetaDataService.getAllMetadata(bucketName,objectName))
        } catch(e: Exception) {
            return ResponseEntity.badRequest().body(UniversalResponseBody().put("status", e.cause!!.message).build())
        }
    }





}