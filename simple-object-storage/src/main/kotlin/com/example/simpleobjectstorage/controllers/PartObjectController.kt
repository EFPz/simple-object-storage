package com.example.simpleobjectstorage.controllers

import com.example.simpleobjectstorage.modules.`object`.ObjectStorageRepository
import com.example.simpleobjectstorage.modules.bucket.BucketRepository
import com.example.simpleobjectstorage.modules.part.PartObjectRepository
import com.example.simpleobjectstorage.modules.part.PartObjectService
import com.example.simpleobjectstorage.utils.UniversalResponseBody
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*


@RestController
@RequestMapping(value = ["{bucketname}/{objectname}"])
@CrossOrigin
class PartObjectController {


    @Autowired
    lateinit var partObjectService: PartObjectService

    @Autowired
    lateinit var partObjectRepository: PartObjectRepository

    @Autowired
    lateinit var objectStorageRepository: ObjectStorageRepository

    @Autowired
    lateinit var bucketRepository: BucketRepository

    @RequestMapping(params = [("listSort")],method = [(RequestMethod.GET)])
    fun list(@PathVariable("bucketname") bucketName: String,
             @PathVariable("objectname") objectName: String): ResponseEntity<Any> {
        return try{
            val bucket = bucketRepository.findByName(bucketName)!!
            val obj = objectStorageRepository.findOneByBucketAndName(bucket,objectName)!!
            ResponseEntity.ok(UniversalResponseBody()
                    .put("List",partObjectRepository.findAllByObjectStorageOrderByPartNumberAsc(obj))
                    .build())
        }catch (e: Exception){
            ResponseEntity.badRequest().body(UniversalResponseBody()
                    .put("error",e.cause!!.message)
                    .put("status",400)
                    .build())
        }
    }

    @RequestMapping(params = ["partNumber"],method = [(RequestMethod.DELETE)])
    fun deletePart(@PathVariable("bucketname") bucketName: String,
                   @PathVariable("objectname") objectName: String,
                   @RequestParam("partNumber") partNumber: Int): ResponseEntity<Any> {
        return try {
            val obj = partObjectService.deletePart(bucketName,objectName,partNumber)
            return ResponseEntity.ok("")
        }catch (e: Exception){
            ResponseEntity.badRequest().body(UniversalResponseBody()
                    .put("name",objectName)
                    .put("error",e.cause!!.message)
                    .build())

        }

    }

}