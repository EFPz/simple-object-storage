package com.example.simpleobjectstorage.modules.`object`.dto

class ObjectUploadInfoResponseDtoFailed(){

    //    json { "md5": {md5}, "length": 1024, "partNumber": 1 "error": "LengthMismatched|MD5Mismatched|InvalidPartNumber|InvalidObjectName|InvalidBucket" }
    var md5 : String? = null

    var length : Int? = null

    var partNumber: Int? = null

    var error: String? = null

    constructor(md5: String,length: Int, partNumber: Int,error : String): this(){

        this.md5 = md5
        this.length = length
        this.partNumber = partNumber
        this.error = error

    }

}