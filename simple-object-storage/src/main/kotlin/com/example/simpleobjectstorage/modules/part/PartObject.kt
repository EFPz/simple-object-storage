package com.example.simpleobjectstorage.modules.part

import com.example.simpleobjectstorage.modules.`object`.ObjectStorage
import com.example.simpleobjectstorage.utils.BaseEntity
import com.fasterxml.jackson.annotation.JsonBackReference
import org.hibernate.annotations.GenericGenerator
import javax.persistence.*


@Entity
class PartObject :BaseEntity() {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator="native")
    @GenericGenerator(name = "native",  strategy = "native")
    var id: Long? = null

    var name: String? = null

    var partNumber: Int? = null

    var length: Int?  = null

    var location: String? =null

    var md5: String? = null


    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "objectStorageId")
    @JsonBackReference
    var objectStorage: ObjectStorage? = null


}