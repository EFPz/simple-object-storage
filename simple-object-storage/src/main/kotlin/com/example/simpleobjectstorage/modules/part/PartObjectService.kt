package com.example.simpleobjectstorage.modules.part

import com.example.simpleobjectstorage.modules.`object`.ObjectStorageRepository
import com.example.simpleobjectstorage.modules.bucket.BucketRepository
import com.example.simpleobjectstorage.modules.part.dto.PartCreateDto
import com.example.simpleobjectstorage.modules.part.dto.PartCreateDtoByObjectName
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional


@Service
class PartObjectService {


    @Autowired
    lateinit var bucketRepository: BucketRepository

    @Autowired
    lateinit var partObjectRepository: PartObjectRepository

    @Autowired
    lateinit var objectStorageRepository: ObjectStorageRepository

    @Transactional
    fun create(partCreateDto: PartCreateDto): PartObject{

        val newPart  = PartObject()
        newPart.length = partCreateDto.length
        newPart.name = partCreateDto.name
        newPart.partNumber = partCreateDto.partNumber
        newPart.location = partCreateDto.location
        newPart.md5 = partCreateDto.md5
        val obj = objectStorageRepository.findById(partCreateDto.obj!!).get()
        newPart.objectStorage = obj
        val savedPart = partObjectRepository.save(newPart)
        obj.partObjects.add(savedPart)
        objectStorageRepository.save(obj)

        return savedPart

    }

    @Transactional
    fun createByObjectName(partCreateDto: PartCreateDtoByObjectName): PartObject{

        val newPart  = PartObject()
        newPart.length = partCreateDto.length
        newPart.name = partCreateDto.name
        newPart.partNumber = partCreateDto.partNumber
        newPart.location = partCreateDto.location
        newPart.md5 = partCreateDto.md5
        val bucket = bucketRepository.findByName(partCreateDto.bucketName!!)!!
        val obj = objectStorageRepository.findOneByBucketAndName(bucket,partCreateDto.obj!!)!!
        newPart.objectStorage = obj
        val savedPart = partObjectRepository.save(newPart)
        obj.partObjects.add(savedPart)
        objectStorageRepository.save(obj)

        return savedPart

    }

    @Transactional
    fun deletePart(bucketName:String,objectName:String,partNumber: Int){
//        Bucket Should exist
        val bucket = bucketRepository.findByName(bucketName)
        if(bucket == null){
            throw Exception("Bucket Doesn't Exist!")
        }

//        ObjectStorage Should exist
        val obj = objectStorageRepository.findOneByBucketAndName(bucket,objectName)
        if(obj == null){
            throw Exception("Object Doesn't Exist!")
        }
//        PartObject Should exist
        val part = partObjectRepository.findOneByObjectStorageAndPartNumberAndActiveIsTrue(obj,partNumber)
        if(part == null){
            throw Exception("Object's Part Doesn't Exist, or has already been deleted")
        }
//      Object shouldn't be marked as completed
        if(obj.complete){
            throw Exception("Can not delete Part of an object that's already been marked as completed")
        }

        part.active = false

//        Archive the Path and Unlink Part from Object
        obj.partObjects.remove(part)
        part.objectStorage = null

        objectStorageRepository.save(obj)
        partObjectRepository.save(part)


    }


}