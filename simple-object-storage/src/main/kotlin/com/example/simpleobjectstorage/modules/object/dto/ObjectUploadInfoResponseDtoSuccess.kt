package com.example.simpleobjectstorage.modules.`object`.dto

class ObjectUploadInfoResponseDtoSuccess() {


    var md5 : String? = null

    var length : Int? = null

    var partNumber: Int? = null

    constructor(md5: String,length: Int, partNumber: Int): this(){

        this.md5 = md5
        this.length = length
        this.partNumber = partNumber

    }

}