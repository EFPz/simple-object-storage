package com.example.simpleobjectstorage.modules.`object`.metadata

import com.example.simpleobjectstorage.modules.`object`.ObjectStorage
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface ObjectMetaDataRepository: JpaRepository<ObjectMetaData, Long> {
    fun findByMetadataKeyAndObjectStorage(metadata: String, objectStorage: ObjectStorage): ObjectMetaData?

    fun findAllByObjectStorage(objectStorage: ObjectStorage): MutableList<ObjectMetaData>?
}