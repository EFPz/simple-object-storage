package com.example.simpleobjectstorage.modules.bucket

import com.example.simpleobjectstorage.modules.bucket.dto.BucketListInfoResponseDto
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import java.io.File
import java.sql.Date
import java.time.Instant

@Service
class BucketService {

    @Autowired
    lateinit var bucketRepository: BucketRepository

    @Transactional
    fun create(name: String): Bucket {
        try {
            if(bucketRepository.findByName(name) != null){
                throw Exception("Bucket already exists")
            }
            val bucket = Bucket()
            bucket.name = name
            File("./buckets/$name").mkdirs()
            return bucketRepository.save(bucket)
        }catch (e : Exception){
            throw e
        }

    }
//TODO Do we delete the actual file or just archive it?
    @Transactional
    fun delete(name: String) {
        try {
            val bucket = bucketRepository.findByName(name)
            if(bucket == null){
                throw Exception("Bucket does not exist.")
            }
            File("./buckets/$name").deleteRecursively()
            bucketRepository.delete(bucket)
        }catch (e : Exception){
            throw Exception(e.message)
        }
    }

    @Transactional
    fun list(name: String): Bucket {
        try{
            val bucket = bucketRepository.findByName(name)
            if (bucket == null) {
                throw Exception("Bucket does not exist.")
            }
            return bucket
        }catch (e: Exception){
            throw Exception(e.message)
        }
    }

//    Disable multipart upload



}