package com.example.simpleobjectstorage.modules.part.dto

import com.example.simpleobjectstorage.modules.part.PartObject

class PartCreateDtoByObjectName(){


    var name: String? = null

    var partNumber: Int? = null

    var length: Int?  = null

    var complete: Boolean =  false

    var obj: String? = null

    var location: String? = null

    var md5 : String? = null

    var bucketName: String? = null

    constructor(partObject: PartObject): this(){
        this.name = partObject.name
        this.partNumber = partObject.partNumber
        this.length = partObject.length
        this.obj = partObject.objectStorage!!.name
        this.location = partObject.location
        this.md5 = partObject.md5
    }
}