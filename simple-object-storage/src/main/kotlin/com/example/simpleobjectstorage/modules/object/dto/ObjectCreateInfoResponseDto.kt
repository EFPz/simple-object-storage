package com.example.simpleobjectstorage.modules.`object`.dto

import com.example.simpleobjectstorage.modules.`object`.ObjectStorage

class ObjectCreateInfoResponseDto() {


    var create : Long? = null

    var modified : Long? = null

    var name : String? = null


    constructor(obj: ObjectStorage) : this() {
        this.create = obj.createdDate!!.time
        this.name = obj.name
        this.modified = obj.modifiedDate!!.time
    }
}