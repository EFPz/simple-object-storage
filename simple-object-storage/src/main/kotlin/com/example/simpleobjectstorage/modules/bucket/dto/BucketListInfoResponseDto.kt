package com.example.simpleobjectstorage.modules.bucket.dto

import com.example.simpleobjectstorage.modules.`object`.dto.ObjectInfoResponsesDto
import com.example.simpleobjectstorage.modules.bucket.Bucket

class BucketListInfoResponseDto() {

    var created: Long? = null
    var modified: Long? = null
    var name: String? = null
    var objects: List<ObjectInfoResponsesDto> = listOf()



    constructor(bucket : Bucket) : this() {
        this.created = bucket.createdDate!!.time
        this.modified = bucket.modifiedDate!!.time
        this.name = bucket.name

        val list = mutableListOf<ObjectInfoResponsesDto>()
        bucket.objectStorages.forEach{
            list.add(ObjectInfoResponsesDto(it))
        }
        this.objects = list

    }
}