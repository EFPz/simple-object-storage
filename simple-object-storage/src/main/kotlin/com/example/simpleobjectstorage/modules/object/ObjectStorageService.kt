package com.example.simpleobjectstorage.modules.`object`

import com.example.simpleobjectstorage.modules.bucket.BucketRepository
import com.example.simpleobjectstorage.modules.part.PartObject
import com.example.simpleobjectstorage.modules.part.PartObjectRepository
import com.sun.org.apache.xpath.internal.operations.Bool
import org.apache.commons.codec.binary.Hex
import org.apache.commons.io.IOUtils
import org.apache.commons.io.input.BoundedInputStream
import org.apache.commons.io.output.ByteArrayOutputStream
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import org.springframework.util.DigestUtils
import java.io.BufferedOutputStream
import java.io.File
import java.io.FileOutputStream
import java.io.InputStream
import javax.servlet.ServletInputStream
import javax.servlet.ServletOutputStream
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse


@Service
class ObjectStorageService {

    @Autowired
    lateinit var objectStorageRepository: ObjectStorageRepository

    @Autowired
    lateinit var bucketRepository: BucketRepository

    @Autowired
    lateinit var partObjectRepository: PartObjectRepository


    @Transactional
    fun create(bucketName: String, objectName: String): ObjectStorage{

        val bucket = bucketRepository.findByName(bucketName)
        if( bucket != null && objectStorageRepository.findOneByBucketAndName(bucket,objectName) == null){
            val newObj = ObjectStorage()
            newObj.name = objectName
            newObj.bucket = bucket
            val savedObj = objectStorageRepository.save(newObj)
            bucket.objectStorages.add(savedObj)
            bucketRepository.save(bucket)
            return savedObj
        }else{
            throw Exception("Bucket doesn't exist or ObjectStorage already exist.")
        }
    }

    @Transactional
    fun getByName(bucketName: String,objectName: String): ObjectStorage? {
        val bucket = bucketRepository.findByName(bucketName)!!
        return objectStorageRepository.findOneByBucketAndName(bucket,objectName)
    }

//bucketName,objectName,partNumber,length,md5,request
    @Transactional
    fun uploadPart(bucketName : String,
                   objectName: String,
                   partNumber: Int,
                   length: Int,
                   md5 : String,
                   request: HttpServletRequest): String{
    try {

        val input = request.inputStream
        val file =  File(objectName)
        val name = file.nameWithoutExtension
        val ext = file.extension
        val location = "buckets/$bucketName/${name + "-" + partNumber.toString()+"."+ext }/"
        val output = FileOutputStream(File(location))

//        while (!input.isFinished){
//            output.write(input.read())
//        }
        output.write(input.readBytes())

        return location

    }catch (e : Exception){
        throw e
        }

    }


    @Transactional
    fun deleteFile(path : String){
        if(!File(path).delete()){
            throw Exception("Couldn't delete the file")
        }
    }



    @Transactional
    fun checkTicketValidity(bucketName : String, objectName: String,partNumber: Int){
//        "LengthMismatched|MD5Mismatched|InvalidPartNumber|InvalidObjectName|InvalidBucket"


//        Check if objectName is in a valid form
        if(objectName[0] == '.' || objectName[objectName.lastIndex] == '.'){
            println("Name starting or ending with '.'")
            throw Exception("InvalidObjectName")
        }

//        Check if the name is not english
//        for(char in objectName){
//            println(char)
//            if ((char !in 'a'..'z') || (char !in 'A'..'Z') || (char !in '0'..'9')) {
//                if(char != '.' || char != '-' || char != '_'){
//                    println("Name have character that isn't in English")
//                    throw Exception("InvalidObjectName")
//                }
//            }
//        }

//        Bucket shouldn't already exist
        val bucket = bucketRepository.findByName(bucketName)
        if(bucket == null){
            println("InvalidBucket")
            throw Exception("InvalidBucket")
        }
//            ObjectStorage should already be exist
        val obj = objectStorageRepository.findOneByBucketAndName(bucket,objectName)
        if(obj == null){
            println("InvalidObjectName")
            throw Exception("InvalidObjectName")
        }
//            ObjectStorage completion flag
        if(obj.complete){
            println("File already completed")
            throw Exception("File already completed")
        }
//        partNumber should be in range 1-10000
        if(partNumber !in 1..10000){
            println("InvalidPartNumber")
            throw Exception("InvalidPartNumber")
        }

//        partObject shouldn't already exist
        if(partObjectRepository.findOneByObjectStorageAndPartNumberAndActiveIsTrue(obj,partNumber) != null){
            println("InvalidPartNumber")
            throw Exception("InvalidPartNumber")
        }

    }

    @Transactional
    fun checkCompleteStatus(bucketName:String, objectName: String){
        val bucket =  bucketRepository.findByName(bucketName)!!
//            Check if ObjectStorage already exist
        val obj = objectStorageRepository.findOneByBucketAndName(bucket,objectName)
        if(obj == null){
            println("InvalidObjectName")
            throw Exception("InvalidObjectName")
        }
//            Check is the ObjectStorage completion flag is complete
        if(obj.complete){
            println("File already completed")
            throw Exception("File already completed")
        }
    }

    @Transactional
    fun checkHeaderValidity(length: Int,md5 : String,location: String){
//            Check if length is the same as body

        var file = File(location)
        var body = file.inputStream()
        val len = IOUtils.toByteArray(body).size
        if(length != len){
            println("LengthMismatched")
            throw Exception("LengthMismatched")
        }
        body.close()
        file = File(location)
        body = file.inputStream()
//            Check is md5 is match
        val bodyMd5Hex  = DigestUtils.md5DigestAsHex(body)
        if(bodyMd5Hex != md5){

            println("MD5Mismatched")
            throw Exception("MD5Mismatched")
        }

        body.close()

    }


    @Transactional
    fun complete(bucketName: String, objectName: String): ObjectStorage {
//        Bucket should exist
        val bucket = bucketRepository.findByName(bucketName) ?: throw Exception("InvalidBucket")

//        ObjectStorage should exist
        val obj = objectStorageRepository.findOneByBucketAndName(bucket,objectName) ?: throw Exception("InvalidObjectName")

//        Object should not be mark completed
        if(obj.complete){
            throw Exception("ObjectStorage already marked as completed.")
        }

//      Calculate and Update New ETag and MD5
       return calculateETagAndMD5(obj)


    }


    @Transactional
    fun calculateETagAndMD5(objectStorage : ObjectStorage): ObjectStorage{

//        Get all parts
        val allParts = objectStorage.partObjects
        val allBytes = arrayListOf<ByteArray>()
        var totalLength = 0
        val totalPart = allParts.size
        for (part in allParts){
            allBytes.add(org.springframework.security.crypto.codec.Hex.decode(part.md5!!))
            totalLength += part.length!!
        }

//        val testHex = arrayListOf(
//                "9961bbe5d7c70a5f0e23d63bc7433b01",
//                "bfbd30c675df62d67f02d0efa72bc1ac",
//                "92589dcb2dd1bcc29ab1ee6b8eb7f6aa")
//        for(hex in testHex){
//            allBytes.add(org.springframework.security.crypto.codec.Hex.decode(hex))
//        }

        objectStorage.totalSize = totalLength
        val concatByte = combineBytes(allBytes)

        val newHexMd5 = DigestUtils.md5DigestAsHex(concatByte)

        objectStorage.md5 = newHexMd5
        objectStorage.objectETag = "$newHexMd5-$totalPart"

        objectStorage.complete = true


        return objectStorageRepository.save(objectStorage)
    }

    @Transactional
    fun combineBytes(allBytes: ArrayList<ByteArray>): InputStream{
        val bops = ByteArrayOutputStream()
        for (bytes in allBytes){
            bops.write(bytes)
        }
        return bops.toInputStream()
    }


    @Transactional
    fun deleteObject(bucketName: String, objectName: String){
//        Bucket Should exist
        val bucket = bucketRepository.findByName(bucketName)
        if(bucket == null){
            throw Exception("Bucket Doesn't Exist!")
        }

//        ObjectStorage Should exist
        val obj = objectStorageRepository.findOneByBucketAndName(bucket,objectName)
        if(obj == null){
            throw Exception("Object Doesn't Exist!")
        }


//        Set completion
        obj.complete = true
//        Archive Object
        obj.active = false

//        Unlink Bucket
        bucket.objectStorages.remove(obj)
        obj.bucket = null


//        Archive all of the Parts and Unlink them all
        val allParts = obj.partObjects
        for(p in allParts){
            p.objectStorage = null
            p.active = false
            partObjectRepository.save(p)
        }

        obj.partObjects = mutableListOf()

        bucketRepository.save(bucket)
        objectStorageRepository.save(obj)

    }

    @Transactional
    fun download(bucketName: String,objectName: String,from: Int,to: Int,response: HttpServletResponse,getAllParts: Boolean){

        val bucket = bucketRepository.findByName(bucketName) ?: throw Exception("Bucket Doesn't Exist")
        val obj = objectStorageRepository.findOneByBucketAndName(bucket,objectName) ?: throw Exception("Object Doesn't Exist")


        if(!obj.complete){
            throw Exception("Object is not completed.")
        }

        val allParts = partObjectRepository.findAllByObjectStorageOrderByPartNumberAsc(obj)
        val target = BufferedOutputStream(response.outputStream)
        val baOutputStream = ByteArrayOutputStream()
        var totalSizeRead = 0
        for (p in allParts){
            val fileInputStream = File(p.location).inputStream()
            totalSizeRead += baOutputStream.write(fileInputStream)
            fileInputStream.close()
        }

//        println(obj.totalSize)
//        println(totalSizeRead)

        val inputStream = baOutputStream.toInputStream()

        if(getAllParts){
//            Write all parts to response
            target.write(inputStream.readBytes())
        }else{

            var newTo = to
            if (newTo < 0){
                newTo = totalSizeRead
            }

            val sizeNeeded = newTo - from
            inputStream.skip(from.toLong())

//            var ba = ByteArray(sizeNeeded)
//            inputStream.read(ba)
//            target.write(ba)

//            val bounded = BoundedInputStream(inputStream,from.toLong()+sizeNeeded.toLong())

            for(i in 1..sizeNeeded){
                target.write(inputStream.read())
            }
//            target.write(bounded.readBytes())
        }

        target.flush()


        response.setHeader("eTag", obj.objectETag)

    }





}