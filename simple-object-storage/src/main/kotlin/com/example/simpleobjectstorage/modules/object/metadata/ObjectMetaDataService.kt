package com.example.simpleobjectstorage.modules.`object`.metadata

import com.example.simpleobjectstorage.modules.`object`.ObjectStorageRepository
import com.example.simpleobjectstorage.modules.bucket.BucketRepository
import com.example.simpleobjectstorage.modules.part.PartObjectRepository
import com.example.simpleobjectstorage.utils.UniversalResponseBody
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

@Service
class ObjectMetaDataService{

    @Autowired
    lateinit var objectStorageRepository: ObjectStorageRepository

    @Autowired
    lateinit var bucketRepository: BucketRepository

    @Autowired
    lateinit var partObjectRepository: PartObjectRepository

    @Autowired
    lateinit var objectMetaDataRepository : ObjectMetaDataRepository



    @Transactional
    fun updateMetadata(bucketName: String, objectName: String, key: String, body: String) {
        try {
            val bucket = bucketRepository.findByName(bucketName)

            //check if bucket doesn't exist -> error
            if (bucket == null) { throw Exception("InvalidBucket: $bucketName doesn't exist") }
            val objectStored = objectStorageRepository.findOneByBucketAndName(bucket,objectName)


            //check if object doesn't exists-> error
            if (objectStored == null) { throw Exception("InvalidObjectName: $objectName doesn't exist") }

            val metadata = objectMetaDataRepository.findByMetadataKeyAndObjectStorage(key,objectStored)

            if (metadata != null ) {
                metadata.body = body
                objectMetaDataRepository.save(metadata)
            } else {
                // add
                var objectMetadata= ObjectMetaData()
                objectMetadata.metadataKey = key
                objectMetadata.body = body
                objectMetadata.objectStorage = objectStored
                objectMetaDataRepository.save(objectMetadata)
                objectStored.metadata.add(objectMetadata)
                objectStorageRepository.save(objectStored)
            }

        } catch (e: Exception) {
            throw Exception(e)
        }
    }

    @Transactional
    fun deleteMetadata(bucketName: String, objectName: String, key: String) {
        try {
            val bucket = bucketRepository.findByName(bucketName)

            //check if bucket doesn't exist -> error
            if (bucket == null) { throw Exception("InvalidBucket: $bucketName doesn't exist") }
            val objectStored = objectStorageRepository.findOneByBucketAndName(bucket,objectName)


            //check if object doesn't exists-> error
            if (objectStored == null) { throw Exception("InvalidObjectName: $objectName doesn't exist") }

            val metadata = objectMetaDataRepository.findByMetadataKeyAndObjectStorage(key,objectStored)
            objectMetaDataRepository.delete(metadata!!)

        } catch (e: Exception) {
            throw Exception(e.message)
        }
    }

    @Transactional
    fun getMetadataByKey(bucketName: String, objectName: String, key: String): UniversalResponseBody {
        try {
            val bucket = bucketRepository.findByName(bucketName)

            //check if bucket doesn't exist -> error
            if (bucket == null) { throw Exception("InvalidBucket: $bucketName doesn't exist") }
            val objectStored = objectStorageRepository.findOneByBucketAndName(bucket,objectName)

            //check if object doesn't exists-> error
            if (objectStored == null) { throw Exception("InvalidObjectName: $objectName doesn't exist") }

            val metadata = objectMetaDataRepository.findByMetadataKeyAndObjectStorage(key,objectStored)
            val metaKey = metadata!!.metadataKey
            val metaVal = metadata!!.body

            return UniversalResponseBody().put(metaKey!!,metaVal)

        } catch (e: Exception) {
            throw Exception(e.message)
        }
    }

    @Transactional
    fun getAllMetadata(bucketName: String, objectName: String): UniversalResponseBody {
        try {
            val bucket = bucketRepository.findByName(bucketName)

            //check if bucket doesn't exist -> error
            if (bucket == null) { throw Exception("InvalidBucket: $bucketName doesn't exist") }
            val objectStored = objectStorageRepository.findOneByBucketAndName(bucket,objectName)

            //check if object doesn't exists-> error
            if (objectStored == null) { throw Exception("InvalidObjectName: $objectName doesn't exist") }

            var metedatas = objectMetaDataRepository.findAllByObjectStorage(objectStored)

            var res = UniversalResponseBody()
            for (meta in metedatas!!) {
                res.put(meta.metadataKey!!, meta.body)
            }

            return res

        } catch (e: Exception) {
            throw Exception(e.message)
        }
    }
}