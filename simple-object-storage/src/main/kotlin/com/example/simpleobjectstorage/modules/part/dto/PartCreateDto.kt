package com.example.simpleobjectstorage.modules.part.dto

import com.example.simpleobjectstorage.modules.part.PartObject

class PartCreateDto() {

    var name: String? = null

    var partNumber: Int? = null

    var length: Int?  = null

    var complete: Boolean =  false

    var location: String? = null

    var obj: Long? = null

    var md5 : String? = null

    constructor(partObject: PartObject): this(){
        this.name = partObject.name
        this.partNumber = partObject.partNumber
        this.length = partObject.length
        this.location = partObject.location
        this.obj = partObject.objectStorage!!.id
        this.md5 = partObject.md5
    }
}