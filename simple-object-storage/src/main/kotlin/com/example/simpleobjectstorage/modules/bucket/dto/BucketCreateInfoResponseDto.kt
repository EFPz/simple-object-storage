package com.example.simpleobjectstorage.modules.bucket.dto

import com.example.simpleobjectstorage.modules.bucket.Bucket
import java.time.Instant
import java.util.*

class BucketCreateInfoResponseDto() {

    var create : Long? = null

    var modified : Long? = null

    var name : String? = null


    constructor(bucket: Bucket) : this() {
        this.create = bucket.createdDate!!.time
        this.name = bucket.name
        this.modified = bucket.modifiedDate!!.time
    }

}