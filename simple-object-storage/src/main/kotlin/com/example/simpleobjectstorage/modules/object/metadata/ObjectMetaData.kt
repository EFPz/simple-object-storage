package com.example.simpleobjectstorage.modules.`object`.metadata

import com.example.simpleobjectstorage.modules.`object`.ObjectStorage
import com.example.simpleobjectstorage.utils.BaseEntity
import com.fasterxml.jackson.annotation.JsonBackReference
import org.hibernate.annotations.GenericGenerator
import javax.persistence.*

@Entity
class ObjectMetaData: BaseEntity() {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator="native")
    @GenericGenerator(name = "native",  strategy = "native")
    var id: Long? = null

    var metadataKey: String? = null

    var body: String? = null

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "objectId")
    @JsonBackReference
    var objectStorage: ObjectStorage? = null

}