package com.example.simpleobjectstorage.modules.bucket

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface BucketRepository : JpaRepository<Bucket, Long> {

    fun findByName(name: String): Bucket?

}