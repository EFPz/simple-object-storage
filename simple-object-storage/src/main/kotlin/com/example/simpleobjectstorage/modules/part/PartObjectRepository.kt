package com.example.simpleobjectstorage.modules.part

import com.example.simpleobjectstorage.modules.`object`.ObjectStorage
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface PartObjectRepository : JpaRepository<PartObject, Long> {

    fun findOneByObjectStorageAndPartNumberAndActiveIsTrue (objectStorage: ObjectStorage, partNumber : Int) : PartObject?

    fun findAllByObjectStorageOrderByPartNumberAsc(objectStorage: ObjectStorage): List<PartObject>


}