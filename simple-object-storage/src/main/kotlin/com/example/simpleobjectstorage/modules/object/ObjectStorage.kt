package com.example.simpleobjectstorage.modules.`object`

import com.example.simpleobjectstorage.modules.`object`.metadata.ObjectMetaData
import com.example.simpleobjectstorage.modules.bucket.Bucket
import com.example.simpleobjectstorage.modules.part.PartObject
import com.example.simpleobjectstorage.utils.BaseEntity
import com.fasterxml.jackson.annotation.JsonBackReference
import com.fasterxml.jackson.annotation.JsonManagedReference
import org.hibernate.annotations.GenericGenerator
import javax.persistence.*


@Entity
class ObjectStorage : BaseEntity() {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator="native")
    @GenericGenerator(name = "native",  strategy = "native")
    var id: Long? = null


    var name: String? = null

    var objectETag: String? = null

    var complete: Boolean = false

    var md5 : String? = null

    var totalSize : Int? = null

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "bucketId")
    @JsonBackReference
    var bucket: Bucket? = null

    @OneToMany(mappedBy = "objectStorage", cascade = [CascadeType.ALL])
    @JsonManagedReference
    var partObjects: MutableList<PartObject> = mutableListOf()

    @OneToMany(mappedBy = "objectStorage", cascade = [CascadeType.ALL])
    @JsonManagedReference
    var metadata: MutableList<ObjectMetaData> = mutableListOf()

}