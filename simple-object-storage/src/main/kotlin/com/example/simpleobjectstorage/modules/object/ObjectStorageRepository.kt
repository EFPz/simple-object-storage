package com.example.simpleobjectstorage.modules.`object`

import com.example.simpleobjectstorage.modules.bucket.Bucket
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository


@Repository
interface ObjectStorageRepository : JpaRepository<ObjectStorage,Long> {


    fun findByName(name: String): ObjectStorage?

    fun findOneByBucketAndName(bucket:Bucket,ame: String): ObjectStorage?

}