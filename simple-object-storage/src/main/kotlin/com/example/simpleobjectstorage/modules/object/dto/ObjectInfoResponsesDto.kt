package com.example.simpleobjectstorage.modules.`object`.dto

import com.example.simpleobjectstorage.modules.`object`.ObjectStorage

class ObjectInfoResponsesDto() {

    //"objectStorages": [ { "name": "access.log", "eTag": {object-eTag}, "created": 1517138542630, "modified": 1517138542630, }


    var name: String? = null
    var objectETag : String? = null
    var created: Long? = null
    var modified: Long? = null
    var md5 : String? = null
    var totalSize : Int? = null

    constructor(obj : ObjectStorage): this(){
        this.name = obj.name
        this.created = obj.createdDate!!.time
        this.modified = obj.modifiedDate!!.time
        this.totalSize = obj.totalSize
        this.md5 = obj.md5
        this.objectETag = obj.objectETag
    }
}