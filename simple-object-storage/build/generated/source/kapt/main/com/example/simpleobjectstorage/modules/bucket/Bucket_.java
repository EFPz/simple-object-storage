package com.example.simpleobjectstorage.modules.bucket;

import com.example.simpleobjectstorage.modules.object.ObjectStorage;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Bucket.class)
public abstract class Bucket_ extends com.example.simpleobjectstorage.utils.BaseEntity_ {

	public static volatile ListAttribute<Bucket, ObjectStorage> objectStorages;
	public static volatile SingularAttribute<Bucket, String> name;
	public static volatile SingularAttribute<Bucket, Long> id;

}

