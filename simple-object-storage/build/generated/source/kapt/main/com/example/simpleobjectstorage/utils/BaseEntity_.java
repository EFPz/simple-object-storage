package com.example.simpleobjectstorage.utils;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(BaseEntity.class)
public abstract class BaseEntity_ {

	public static volatile SingularAttribute<BaseEntity, Date> createdDate;
	public static volatile SingularAttribute<BaseEntity, String> createdBy;
	public static volatile SingularAttribute<BaseEntity, Date> modifiedDate;
	public static volatile SingularAttribute<BaseEntity, Boolean> active;
	public static volatile SingularAttribute<BaseEntity, String> modifiedBy;
	public static volatile SingularAttribute<BaseEntity, Long> version;

}

