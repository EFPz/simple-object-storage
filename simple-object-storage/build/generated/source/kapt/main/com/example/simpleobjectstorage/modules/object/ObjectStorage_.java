package com.example.simpleobjectstorage.modules.object;

import com.example.simpleobjectstorage.modules.bucket.Bucket;
import com.example.simpleobjectstorage.modules.object.metadata.ObjectMetaData;
import com.example.simpleobjectstorage.modules.part.PartObject;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(ObjectStorage.class)
public abstract class ObjectStorage_ extends com.example.simpleobjectstorage.utils.BaseEntity_ {

	public static volatile SingularAttribute<ObjectStorage, Bucket> bucket;
	public static volatile ListAttribute<ObjectStorage, PartObject> partObjects;
	public static volatile ListAttribute<ObjectStorage, ObjectMetaData> metadata;
	public static volatile SingularAttribute<ObjectStorage, Integer> totalSize;
	public static volatile SingularAttribute<ObjectStorage, String> name;
	public static volatile SingularAttribute<ObjectStorage, Long> id;
	public static volatile SingularAttribute<ObjectStorage, String> objectETag;
	public static volatile SingularAttribute<ObjectStorage, Boolean> complete;
	public static volatile SingularAttribute<ObjectStorage, String> md5;

}

