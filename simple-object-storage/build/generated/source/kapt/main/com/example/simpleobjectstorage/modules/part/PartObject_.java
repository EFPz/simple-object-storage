package com.example.simpleobjectstorage.modules.part;

import com.example.simpleobjectstorage.modules.object.ObjectStorage;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(PartObject.class)
public abstract class PartObject_ extends com.example.simpleobjectstorage.utils.BaseEntity_ {

	public static volatile SingularAttribute<PartObject, String> name;
	public static volatile SingularAttribute<PartObject, Integer> length;
	public static volatile SingularAttribute<PartObject, ObjectStorage> objectStorage;
	public static volatile SingularAttribute<PartObject, Integer> partNumber;
	public static volatile SingularAttribute<PartObject, String> location;
	public static volatile SingularAttribute<PartObject, Long> id;
	public static volatile SingularAttribute<PartObject, String> md5;

}

