package com.example.simpleobjectstorage.modules.object.metadata;

import com.example.simpleobjectstorage.modules.object.ObjectStorage;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(ObjectMetaData.class)
public abstract class ObjectMetaData_ extends com.example.simpleobjectstorage.utils.BaseEntity_ {

	public static volatile SingularAttribute<ObjectMetaData, String> metadataKey;
	public static volatile SingularAttribute<ObjectMetaData, ObjectStorage> objectStorage;
	public static volatile SingularAttribute<ObjectMetaData, Long> id;
	public static volatile SingularAttribute<ObjectMetaData, String> body;

}

