package com.example.simpleobjectstorage.modules.bucket.dto;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 10}, bv = {1, 0, 2}, k = 1, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\t\n\u0002\b\t\n\u0002\u0010\u000e\n\u0002\b\u0005\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u00002\u00020\u0001B\u000f\b\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004B\u0005\u00a2\u0006\u0002\u0010\u0005R\u001e\u0010\u0006\u001a\u0004\u0018\u00010\u0007X\u0086\u000e\u00a2\u0006\u0010\n\u0002\u0010\f\u001a\u0004\b\b\u0010\t\"\u0004\b\n\u0010\u000bR\u001e\u0010\r\u001a\u0004\u0018\u00010\u0007X\u0086\u000e\u00a2\u0006\u0010\n\u0002\u0010\f\u001a\u0004\b\u000e\u0010\t\"\u0004\b\u000f\u0010\u000bR\u001c\u0010\u0010\u001a\u0004\u0018\u00010\u0011X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0012\u0010\u0013\"\u0004\b\u0014\u0010\u0015R \u0010\u0016\u001a\b\u0012\u0004\u0012\u00020\u00180\u0017X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0019\u0010\u001a\"\u0004\b\u001b\u0010\u001c\u00a8\u0006\u001d"}, d2 = {"Lcom/example/simpleobjectstorage/modules/bucket/dto/BucketListInfoResponseDto;", "", "bucket", "Lcom/example/simpleobjectstorage/modules/bucket/Bucket;", "(Lcom/example/simpleobjectstorage/modules/bucket/Bucket;)V", "()V", "created", "", "getCreated", "()Ljava/lang/Long;", "setCreated", "(Ljava/lang/Long;)V", "Ljava/lang/Long;", "modified", "getModified", "setModified", "name", "", "getName", "()Ljava/lang/String;", "setName", "(Ljava/lang/String;)V", "objects", "", "Lcom/example/simpleobjectstorage/modules/object/dto/ObjectInfoResponsesDto;", "getObjects", "()Ljava/util/List;", "setObjects", "(Ljava/util/List;)V", "simple-object-storage"})
public final class BucketListInfoResponseDto {
    @org.jetbrains.annotations.Nullable()
    private java.lang.Long created;
    @org.jetbrains.annotations.Nullable()
    private java.lang.Long modified;
    @org.jetbrains.annotations.Nullable()
    private java.lang.String name;
    @org.jetbrains.annotations.NotNull()
    private java.util.List<com.example.simpleobjectstorage.modules.object.dto.ObjectInfoResponsesDto> objects;
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Long getCreated() {
        return null;
    }
    
    public final void setCreated(@org.jetbrains.annotations.Nullable()
    java.lang.Long p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Long getModified() {
        return null;
    }
    
    public final void setModified(@org.jetbrains.annotations.Nullable()
    java.lang.Long p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getName() {
        return null;
    }
    
    public final void setName(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.List<com.example.simpleobjectstorage.modules.object.dto.ObjectInfoResponsesDto> getObjects() {
        return null;
    }
    
    public final void setObjects(@org.jetbrains.annotations.NotNull()
    java.util.List<com.example.simpleobjectstorage.modules.object.dto.ObjectInfoResponsesDto> p0) {
    }
    
    public BucketListInfoResponseDto() {
        super();
    }
    
    public BucketListInfoResponseDto(@org.jetbrains.annotations.NotNull()
    com.example.simpleobjectstorage.modules.bucket.Bucket bucket) {
        super();
    }
}