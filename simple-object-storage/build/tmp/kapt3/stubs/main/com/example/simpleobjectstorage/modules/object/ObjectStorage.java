package com.example.simpleobjectstorage.modules.object;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 10}, bv = {1, 0, 2}, k = 1, d1 = {"\u0000H\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000b\n\u0002\b\u0005\n\u0002\u0010\t\n\u0002\b\u0006\n\u0002\u0010\u000e\n\u0002\b\u0005\n\u0002\u0010!\n\u0002\u0018\u0002\n\u0002\b\u000b\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0006\b\u0007\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002R \u0010\u0003\u001a\u0004\u0018\u00010\u00048\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0005\u0010\u0006\"\u0004\b\u0007\u0010\bR\u001a\u0010\t\u001a\u00020\nX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u000b\u0010\f\"\u0004\b\r\u0010\u000eR\"\u0010\u000f\u001a\u0004\u0018\u00010\u00108\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0010\n\u0002\u0010\u0015\u001a\u0004\b\u0011\u0010\u0012\"\u0004\b\u0013\u0010\u0014R\u001c\u0010\u0016\u001a\u0004\u0018\u00010\u0017X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0018\u0010\u0019\"\u0004\b\u001a\u0010\u001bR$\u0010\u001c\u001a\b\u0012\u0004\u0012\u00020\u001e0\u001d8\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u001f\u0010 \"\u0004\b!\u0010\"R\u001c\u0010#\u001a\u0004\u0018\u00010\u0017X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b$\u0010\u0019\"\u0004\b%\u0010\u001bR\u001c\u0010&\u001a\u0004\u0018\u00010\u0017X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\'\u0010\u0019\"\u0004\b(\u0010\u001bR$\u0010)\u001a\b\u0012\u0004\u0012\u00020*0\u001d8\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b+\u0010 \"\u0004\b,\u0010\"R\u001e\u0010-\u001a\u0004\u0018\u00010.X\u0086\u000e\u00a2\u0006\u0010\n\u0002\u00103\u001a\u0004\b/\u00100\"\u0004\b1\u00102\u00a8\u00064"}, d2 = {"Lcom/example/simpleobjectstorage/modules/object/ObjectStorage;", "Lcom/example/simpleobjectstorage/utils/BaseEntity;", "()V", "bucket", "Lcom/example/simpleobjectstorage/modules/bucket/Bucket;", "getBucket", "()Lcom/example/simpleobjectstorage/modules/bucket/Bucket;", "setBucket", "(Lcom/example/simpleobjectstorage/modules/bucket/Bucket;)V", "complete", "", "getComplete", "()Z", "setComplete", "(Z)V", "id", "", "getId", "()Ljava/lang/Long;", "setId", "(Ljava/lang/Long;)V", "Ljava/lang/Long;", "md5", "", "getMd5", "()Ljava/lang/String;", "setMd5", "(Ljava/lang/String;)V", "metadata", "", "Lcom/example/simpleobjectstorage/modules/object/metadata/ObjectMetaData;", "getMetadata", "()Ljava/util/List;", "setMetadata", "(Ljava/util/List;)V", "name", "getName", "setName", "objectETag", "getObjectETag", "setObjectETag", "partObjects", "Lcom/example/simpleobjectstorage/modules/part/PartObject;", "getPartObjects", "setPartObjects", "totalSize", "", "getTotalSize", "()Ljava/lang/Integer;", "setTotalSize", "(Ljava/lang/Integer;)V", "Ljava/lang/Integer;", "simple-object-storage"})
@javax.persistence.Entity()
public final class ObjectStorage extends com.example.simpleobjectstorage.utils.BaseEntity {
    @org.jetbrains.annotations.Nullable()
    @org.hibernate.annotations.GenericGenerator(name = "native", strategy = "native")
    @javax.persistence.GeneratedValue(strategy = javax.persistence.GenerationType.AUTO, generator = "native")
    @javax.persistence.Id()
    private java.lang.Long id;
    @org.jetbrains.annotations.Nullable()
    private java.lang.String name;
    @org.jetbrains.annotations.Nullable()
    private java.lang.String objectETag;
    private boolean complete;
    @org.jetbrains.annotations.Nullable()
    private java.lang.String md5;
    @org.jetbrains.annotations.Nullable()
    private java.lang.Integer totalSize;
    @org.jetbrains.annotations.Nullable()
    @com.fasterxml.jackson.annotation.JsonBackReference()
    @javax.persistence.JoinColumn(name = "bucketId")
    @javax.persistence.ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private com.example.simpleobjectstorage.modules.bucket.Bucket bucket;
    @org.jetbrains.annotations.NotNull()
    @com.fasterxml.jackson.annotation.JsonManagedReference()
    @javax.persistence.OneToMany(mappedBy = "objectStorage", cascade = {javax.persistence.CascadeType.ALL})
    private java.util.List<com.example.simpleobjectstorage.modules.part.PartObject> partObjects;
    @org.jetbrains.annotations.NotNull()
    @com.fasterxml.jackson.annotation.JsonManagedReference()
    @javax.persistence.OneToMany(mappedBy = "objectStorage", cascade = {javax.persistence.CascadeType.ALL})
    private java.util.List<com.example.simpleobjectstorage.modules.object.metadata.ObjectMetaData> metadata;
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Long getId() {
        return null;
    }
    
    public final void setId(@org.jetbrains.annotations.Nullable()
    java.lang.Long p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getName() {
        return null;
    }
    
    public final void setName(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getObjectETag() {
        return null;
    }
    
    public final void setObjectETag(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
    
    public final boolean getComplete() {
        return false;
    }
    
    public final void setComplete(boolean p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getMd5() {
        return null;
    }
    
    public final void setMd5(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Integer getTotalSize() {
        return null;
    }
    
    public final void setTotalSize(@org.jetbrains.annotations.Nullable()
    java.lang.Integer p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.example.simpleobjectstorage.modules.bucket.Bucket getBucket() {
        return null;
    }
    
    public final void setBucket(@org.jetbrains.annotations.Nullable()
    com.example.simpleobjectstorage.modules.bucket.Bucket p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.List<com.example.simpleobjectstorage.modules.part.PartObject> getPartObjects() {
        return null;
    }
    
    public final void setPartObjects(@org.jetbrains.annotations.NotNull()
    java.util.List<com.example.simpleobjectstorage.modules.part.PartObject> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.List<com.example.simpleobjectstorage.modules.object.metadata.ObjectMetaData> getMetadata() {
        return null;
    }
    
    public final void setMetadata(@org.jetbrains.annotations.NotNull()
    java.util.List<com.example.simpleobjectstorage.modules.object.metadata.ObjectMetaData> p0) {
    }
    
    public ObjectStorage() {
        super();
    }
}