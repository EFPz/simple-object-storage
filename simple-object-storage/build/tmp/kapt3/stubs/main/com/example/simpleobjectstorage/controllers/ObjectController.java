package com.example.simpleobjectstorage.controllers;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 10}, bv = {1, 0, 2}, k = 1, d1 = {"\u0000R\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0010\b\n\u0002\b\u0003\b\u0017\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\"\u0010\u001b\u001a\b\u0012\u0004\u0012\u00020\u00010\u001c2\b\b\u0001\u0010\u001d\u001a\u00020\u001e2\b\b\u0001\u0010\u001f\u001a\u00020\u001eH\u0017J\"\u0010 \u001a\b\u0012\u0004\u0012\u00020\u00010\u001c2\b\b\u0001\u0010\u001d\u001a\u00020\u001e2\b\b\u0001\u0010\u001f\u001a\u00020\u001eH\u0017J4\u0010!\u001a\b\u0012\u0004\u0012\u00020\u00010\u001c2\b\b\u0001\u0010\u001d\u001a\u00020\u001e2\b\b\u0001\u0010\u001f\u001a\u00020\u001e2\b\b\u0001\u0010\"\u001a\u00020\u001e2\u0006\u0010#\u001a\u00020$H\u0017J\"\u0010%\u001a\b\u0012\u0004\u0012\u00020\u00010\u001c2\b\b\u0001\u0010\u001d\u001a\u00020\u001e2\b\b\u0001\u0010\u001f\u001a\u00020\u001eH\u0017J<\u0010&\u001a\b\u0012\u0004\u0012\u00020\u00010\u001c2\b\b\u0001\u0010\u001d\u001a\u00020\u001e2\b\b\u0001\u0010\u001f\u001a\u00020\u001e2\b\b\u0001\u0010\'\u001a\u00020\u001e2\u0006\u0010#\u001a\u00020$2\u0006\u0010(\u001a\u00020)H\u0017J*\u0010*\u001a\b\u0012\u0004\u0012\u00020\u00010\u001c2\b\b\u0001\u0010\u001d\u001a\u00020\u001e2\b\b\u0001\u0010\u001f\u001a\u00020\u001e2\u0006\u0010#\u001a\u00020$H\u0017J4\u0010+\u001a\b\u0012\u0004\u0012\u00020\u00010\u001c2\b\b\u0001\u0010\u001d\u001a\u00020\u001e2\b\b\u0001\u0010\u001f\u001a\u00020\u001e2\b\b\u0001\u0010\"\u001a\u00020\u001e2\u0006\u0010#\u001a\u00020$H\u0017J\"\u0010,\u001a\b\u0012\u0004\u0012\u00020\u00010\u001c2\b\b\u0001\u0010\u001d\u001a\u00020\u001e2\b\b\u0001\u0010\u001f\u001a\u00020\u001eH\u0017J>\u0010-\u001a\b\u0012\u0004\u0012\u00020\u00010\u001c2\b\b\u0001\u0010\u001d\u001a\u00020\u001e2\b\b\u0001\u0010\u001f\u001a\u00020\u001e2\b\b\u0001\u0010\"\u001a\u00020\u001e2\b\b\u0001\u0010.\u001a\u00020\u001e2\u0006\u0010#\u001a\u00020$H\u0017JH\u0010/\u001a\b\u0012\u0004\u0012\u00020\u00010\u001c2\b\b\u0001\u0010\u001d\u001a\u00020\u001e2\b\b\u0001\u0010\u001f\u001a\u00020\u001e2\b\b\u0001\u00100\u001a\u0002012\b\b\u0001\u00102\u001a\u0002012\b\b\u0001\u00103\u001a\u00020\u001e2\u0006\u0010#\u001a\u00020$H\u0017R\u001e\u0010\u0003\u001a\u00020\u00048\u0016@\u0016X\u0097.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0005\u0010\u0006\"\u0004\b\u0007\u0010\bR\u001e\u0010\t\u001a\u00020\n8\u0016@\u0016X\u0097.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u000b\u0010\f\"\u0004\b\r\u0010\u000eR\u001e\u0010\u000f\u001a\u00020\u00108\u0016@\u0016X\u0097.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0011\u0010\u0012\"\u0004\b\u0013\u0010\u0014R\u001e\u0010\u0015\u001a\u00020\u00168\u0016@\u0016X\u0097.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0017\u0010\u0018\"\u0004\b\u0019\u0010\u001a\u00a8\u00064"}, d2 = {"Lcom/example/simpleobjectstorage/controllers/ObjectController;", "", "()V", "objectMetaDataService", "Lcom/example/simpleobjectstorage/modules/object/metadata/ObjectMetaDataService;", "getObjectMetaDataService", "()Lcom/example/simpleobjectstorage/modules/object/metadata/ObjectMetaDataService;", "setObjectMetaDataService", "(Lcom/example/simpleobjectstorage/modules/object/metadata/ObjectMetaDataService;)V", "objectStorageRepository", "Lcom/example/simpleobjectstorage/modules/object/ObjectStorageRepository;", "getObjectStorageRepository", "()Lcom/example/simpleobjectstorage/modules/object/ObjectStorageRepository;", "setObjectStorageRepository", "(Lcom/example/simpleobjectstorage/modules/object/ObjectStorageRepository;)V", "objectStorageService", "Lcom/example/simpleobjectstorage/modules/object/ObjectStorageService;", "getObjectStorageService", "()Lcom/example/simpleobjectstorage/modules/object/ObjectStorageService;", "setObjectStorageService", "(Lcom/example/simpleobjectstorage/modules/object/ObjectStorageService;)V", "partObjectService", "Lcom/example/simpleobjectstorage/modules/part/PartObjectService;", "getPartObjectService", "()Lcom/example/simpleobjectstorage/modules/part/PartObjectService;", "setPartObjectService", "(Lcom/example/simpleobjectstorage/modules/part/PartObjectService;)V", "complete", "Lorg/springframework/http/ResponseEntity;", "bucketName", "", "objectName", "createObject", "deleteMetadata", "key", "request", "Ljavax/servlet/http/HttpServletRequest;", "deleteObject", "download", "range", "response", "Ljavax/servlet/http/HttpServletResponse;", "getAllMetadata", "getMetadataByKey", "list", "updateMetadata", "body", "uploadAllParts", "partNumber", "", "length", "md5", "simple-object-storage"})
@org.springframework.web.bind.annotation.CrossOrigin()
@org.springframework.web.bind.annotation.RequestMapping(value = {"{bucketname}/{objectname}"})
@org.springframework.web.bind.annotation.RestController()
public class ObjectController {
    @org.jetbrains.annotations.NotNull()
    @org.springframework.beans.factory.annotation.Autowired()
    public com.example.simpleobjectstorage.modules.object.ObjectStorageRepository objectStorageRepository;
    @org.jetbrains.annotations.NotNull()
    @org.springframework.beans.factory.annotation.Autowired()
    public com.example.simpleobjectstorage.modules.object.ObjectStorageService objectStorageService;
    @org.jetbrains.annotations.NotNull()
    @org.springframework.beans.factory.annotation.Autowired()
    public com.example.simpleobjectstorage.modules.part.PartObjectService partObjectService;
    @org.jetbrains.annotations.NotNull()
    @org.springframework.beans.factory.annotation.Autowired()
    public com.example.simpleobjectstorage.modules.object.metadata.ObjectMetaDataService objectMetaDataService;
    
    @org.jetbrains.annotations.NotNull()
    public com.example.simpleobjectstorage.modules.object.ObjectStorageRepository getObjectStorageRepository() {
        return null;
    }
    
    public void setObjectStorageRepository(@org.jetbrains.annotations.NotNull()
    com.example.simpleobjectstorage.modules.object.ObjectStorageRepository p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public com.example.simpleobjectstorage.modules.object.ObjectStorageService getObjectStorageService() {
        return null;
    }
    
    public void setObjectStorageService(@org.jetbrains.annotations.NotNull()
    com.example.simpleobjectstorage.modules.object.ObjectStorageService p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public com.example.simpleobjectstorage.modules.part.PartObjectService getPartObjectService() {
        return null;
    }
    
    public void setPartObjectService(@org.jetbrains.annotations.NotNull()
    com.example.simpleobjectstorage.modules.part.PartObjectService p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public com.example.simpleobjectstorage.modules.object.metadata.ObjectMetaDataService getObjectMetaDataService() {
        return null;
    }
    
    public void setObjectMetaDataService(@org.jetbrains.annotations.NotNull()
    com.example.simpleobjectstorage.modules.object.metadata.ObjectMetaDataService p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    @org.springframework.web.bind.annotation.RequestMapping(params = {"create"}, method = {org.springframework.web.bind.annotation.RequestMethod.POST})
    public org.springframework.http.ResponseEntity<java.lang.Object> createObject(@org.jetbrains.annotations.NotNull()
    @org.springframework.web.bind.annotation.PathVariable(value = "bucketname")
    java.lang.String bucketName, @org.jetbrains.annotations.NotNull()
    @org.springframework.web.bind.annotation.PathVariable(value = "objectname")
    java.lang.String objectName) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @org.springframework.web.bind.annotation.RequestMapping(method = {org.springframework.web.bind.annotation.RequestMethod.PUT})
    public org.springframework.http.ResponseEntity<java.lang.Object> uploadAllParts(@org.jetbrains.annotations.NotNull()
    @org.springframework.web.bind.annotation.PathVariable(value = "bucketname")
    java.lang.String bucketName, @org.jetbrains.annotations.NotNull()
    @org.springframework.web.bind.annotation.PathVariable(value = "objectname")
    java.lang.String objectName, @org.springframework.web.bind.annotation.RequestParam(value = "partNumber")
    int partNumber, @org.springframework.web.bind.annotation.RequestHeader(value = "Content-Length")
    int length, @org.jetbrains.annotations.NotNull()
    @org.springframework.web.bind.annotation.RequestHeader(value = "Content-MD5")
    java.lang.String md5, @org.jetbrains.annotations.NotNull()
    javax.servlet.http.HttpServletRequest request) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @org.springframework.web.bind.annotation.RequestMapping(params = {"complete"}, method = {org.springframework.web.bind.annotation.RequestMethod.POST})
    public org.springframework.http.ResponseEntity<java.lang.Object> complete(@org.jetbrains.annotations.NotNull()
    @org.springframework.web.bind.annotation.PathVariable(value = "bucketname")
    java.lang.String bucketName, @org.jetbrains.annotations.NotNull()
    @org.springframework.web.bind.annotation.PathVariable(value = "objectname")
    java.lang.String objectName) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @org.springframework.web.bind.annotation.RequestMapping(method = {org.springframework.web.bind.annotation.RequestMethod.DELETE})
    public org.springframework.http.ResponseEntity<java.lang.Object> deleteObject(@org.jetbrains.annotations.NotNull()
    @org.springframework.web.bind.annotation.PathVariable(value = "bucketname")
    java.lang.String bucketName, @org.jetbrains.annotations.NotNull()
    @org.springframework.web.bind.annotation.PathVariable(value = "objectname")
    java.lang.String objectName) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @org.springframework.web.bind.annotation.RequestMapping(params = {"list"}, method = {org.springframework.web.bind.annotation.RequestMethod.GET})
    public org.springframework.http.ResponseEntity<java.lang.Object> list(@org.jetbrains.annotations.NotNull()
    @org.springframework.web.bind.annotation.PathVariable(value = "bucketname")
    java.lang.String bucketName, @org.jetbrains.annotations.NotNull()
    @org.springframework.web.bind.annotation.PathVariable(value = "objectname")
    java.lang.String objectName) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @org.springframework.web.bind.annotation.RequestMapping(method = {org.springframework.web.bind.annotation.RequestMethod.GET})
    public org.springframework.http.ResponseEntity<java.lang.Object> download(@org.jetbrains.annotations.NotNull()
    @org.springframework.web.bind.annotation.PathVariable(value = "bucketname")
    java.lang.String bucketName, @org.jetbrains.annotations.NotNull()
    @org.springframework.web.bind.annotation.PathVariable(value = "objectname")
    java.lang.String objectName, @org.jetbrains.annotations.NotNull()
    @org.springframework.web.bind.annotation.RequestHeader(value = "Range")
    java.lang.String range, @org.jetbrains.annotations.NotNull()
    javax.servlet.http.HttpServletRequest request, @org.jetbrains.annotations.NotNull()
    javax.servlet.http.HttpServletResponse response) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @org.springframework.web.bind.annotation.RequestMapping(method = {org.springframework.web.bind.annotation.RequestMethod.PUT}, params = {"metadata"})
    public org.springframework.http.ResponseEntity<java.lang.Object> updateMetadata(@org.jetbrains.annotations.NotNull()
    @org.springframework.web.bind.annotation.PathVariable(value = "bucketname")
    java.lang.String bucketName, @org.jetbrains.annotations.NotNull()
    @org.springframework.web.bind.annotation.PathVariable(value = "objectname")
    java.lang.String objectName, @org.jetbrains.annotations.NotNull()
    @org.springframework.web.bind.annotation.RequestParam(value = "key")
    java.lang.String key, @org.jetbrains.annotations.NotNull()
    @org.springframework.web.bind.annotation.RequestBody()
    java.lang.String body, @org.jetbrains.annotations.NotNull()
    javax.servlet.http.HttpServletRequest request) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @org.springframework.web.bind.annotation.RequestMapping(method = {org.springframework.web.bind.annotation.RequestMethod.DELETE}, params = {"metadata"})
    public org.springframework.http.ResponseEntity<java.lang.Object> deleteMetadata(@org.jetbrains.annotations.NotNull()
    @org.springframework.web.bind.annotation.PathVariable(value = "bucketname")
    java.lang.String bucketName, @org.jetbrains.annotations.NotNull()
    @org.springframework.web.bind.annotation.PathVariable(value = "objectname")
    java.lang.String objectName, @org.jetbrains.annotations.NotNull()
    @org.springframework.web.bind.annotation.RequestParam(value = "key")
    java.lang.String key, @org.jetbrains.annotations.NotNull()
    javax.servlet.http.HttpServletRequest request) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @org.springframework.web.bind.annotation.RequestMapping(method = {org.springframework.web.bind.annotation.RequestMethod.GET}, params = {"metadata", "key"})
    public org.springframework.http.ResponseEntity<java.lang.Object> getMetadataByKey(@org.jetbrains.annotations.NotNull()
    @org.springframework.web.bind.annotation.PathVariable(value = "bucketname")
    java.lang.String bucketName, @org.jetbrains.annotations.NotNull()
    @org.springframework.web.bind.annotation.PathVariable(value = "objectname")
    java.lang.String objectName, @org.jetbrains.annotations.NotNull()
    @org.springframework.web.bind.annotation.RequestParam(value = "key")
    java.lang.String key, @org.jetbrains.annotations.NotNull()
    javax.servlet.http.HttpServletRequest request) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @org.springframework.web.bind.annotation.RequestMapping(method = {org.springframework.web.bind.annotation.RequestMethod.GET}, params = {"metadata"})
    public org.springframework.http.ResponseEntity<java.lang.Object> getAllMetadata(@org.jetbrains.annotations.NotNull()
    @org.springframework.web.bind.annotation.PathVariable(value = "bucketname")
    java.lang.String bucketName, @org.jetbrains.annotations.NotNull()
    @org.springframework.web.bind.annotation.PathVariable(value = "objectname")
    java.lang.String objectName, @org.jetbrains.annotations.NotNull()
    javax.servlet.http.HttpServletRequest request) {
        return null;
    }
    
    public ObjectController() {
        super();
    }
}