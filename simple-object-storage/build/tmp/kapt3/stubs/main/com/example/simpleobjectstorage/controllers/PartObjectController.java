package com.example.simpleobjectstorage.controllers;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 10}, bv = {1, 0, 2}, k = 1, d1 = {"\u0000B\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\b\u0017\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J,\u0010\u001b\u001a\b\u0012\u0004\u0012\u00020\u00010\u001c2\b\b\u0001\u0010\u001d\u001a\u00020\u001e2\b\b\u0001\u0010\u001f\u001a\u00020\u001e2\b\b\u0001\u0010 \u001a\u00020!H\u0017J\"\u0010\"\u001a\b\u0012\u0004\u0012\u00020\u00010\u001c2\b\b\u0001\u0010\u001d\u001a\u00020\u001e2\b\b\u0001\u0010\u001f\u001a\u00020\u001eH\u0017R\u001e\u0010\u0003\u001a\u00020\u00048\u0016@\u0016X\u0097.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0005\u0010\u0006\"\u0004\b\u0007\u0010\bR\u001e\u0010\t\u001a\u00020\n8\u0016@\u0016X\u0097.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u000b\u0010\f\"\u0004\b\r\u0010\u000eR\u001e\u0010\u000f\u001a\u00020\u00108\u0016@\u0016X\u0097.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0011\u0010\u0012\"\u0004\b\u0013\u0010\u0014R\u001e\u0010\u0015\u001a\u00020\u00168\u0016@\u0016X\u0097.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0017\u0010\u0018\"\u0004\b\u0019\u0010\u001a\u00a8\u0006#"}, d2 = {"Lcom/example/simpleobjectstorage/controllers/PartObjectController;", "", "()V", "bucketRepository", "Lcom/example/simpleobjectstorage/modules/bucket/BucketRepository;", "getBucketRepository", "()Lcom/example/simpleobjectstorage/modules/bucket/BucketRepository;", "setBucketRepository", "(Lcom/example/simpleobjectstorage/modules/bucket/BucketRepository;)V", "objectStorageRepository", "Lcom/example/simpleobjectstorage/modules/object/ObjectStorageRepository;", "getObjectStorageRepository", "()Lcom/example/simpleobjectstorage/modules/object/ObjectStorageRepository;", "setObjectStorageRepository", "(Lcom/example/simpleobjectstorage/modules/object/ObjectStorageRepository;)V", "partObjectRepository", "Lcom/example/simpleobjectstorage/modules/part/PartObjectRepository;", "getPartObjectRepository", "()Lcom/example/simpleobjectstorage/modules/part/PartObjectRepository;", "setPartObjectRepository", "(Lcom/example/simpleobjectstorage/modules/part/PartObjectRepository;)V", "partObjectService", "Lcom/example/simpleobjectstorage/modules/part/PartObjectService;", "getPartObjectService", "()Lcom/example/simpleobjectstorage/modules/part/PartObjectService;", "setPartObjectService", "(Lcom/example/simpleobjectstorage/modules/part/PartObjectService;)V", "deletePart", "Lorg/springframework/http/ResponseEntity;", "bucketName", "", "objectName", "partNumber", "", "list", "simple-object-storage"})
@org.springframework.web.bind.annotation.CrossOrigin()
@org.springframework.web.bind.annotation.RequestMapping(value = {"{bucketname}/{objectname}"})
@org.springframework.web.bind.annotation.RestController()
public class PartObjectController {
    @org.jetbrains.annotations.NotNull()
    @org.springframework.beans.factory.annotation.Autowired()
    public com.example.simpleobjectstorage.modules.part.PartObjectService partObjectService;
    @org.jetbrains.annotations.NotNull()
    @org.springframework.beans.factory.annotation.Autowired()
    public com.example.simpleobjectstorage.modules.part.PartObjectRepository partObjectRepository;
    @org.jetbrains.annotations.NotNull()
    @org.springframework.beans.factory.annotation.Autowired()
    public com.example.simpleobjectstorage.modules.object.ObjectStorageRepository objectStorageRepository;
    @org.jetbrains.annotations.NotNull()
    @org.springframework.beans.factory.annotation.Autowired()
    public com.example.simpleobjectstorage.modules.bucket.BucketRepository bucketRepository;
    
    @org.jetbrains.annotations.NotNull()
    public com.example.simpleobjectstorage.modules.part.PartObjectService getPartObjectService() {
        return null;
    }
    
    public void setPartObjectService(@org.jetbrains.annotations.NotNull()
    com.example.simpleobjectstorage.modules.part.PartObjectService p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public com.example.simpleobjectstorage.modules.part.PartObjectRepository getPartObjectRepository() {
        return null;
    }
    
    public void setPartObjectRepository(@org.jetbrains.annotations.NotNull()
    com.example.simpleobjectstorage.modules.part.PartObjectRepository p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public com.example.simpleobjectstorage.modules.object.ObjectStorageRepository getObjectStorageRepository() {
        return null;
    }
    
    public void setObjectStorageRepository(@org.jetbrains.annotations.NotNull()
    com.example.simpleobjectstorage.modules.object.ObjectStorageRepository p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public com.example.simpleobjectstorage.modules.bucket.BucketRepository getBucketRepository() {
        return null;
    }
    
    public void setBucketRepository(@org.jetbrains.annotations.NotNull()
    com.example.simpleobjectstorage.modules.bucket.BucketRepository p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    @org.springframework.web.bind.annotation.RequestMapping(params = {"listSort"}, method = {org.springframework.web.bind.annotation.RequestMethod.GET})
    public org.springframework.http.ResponseEntity<java.lang.Object> list(@org.jetbrains.annotations.NotNull()
    @org.springframework.web.bind.annotation.PathVariable(value = "bucketname")
    java.lang.String bucketName, @org.jetbrains.annotations.NotNull()
    @org.springframework.web.bind.annotation.PathVariable(value = "objectname")
    java.lang.String objectName) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @org.springframework.web.bind.annotation.RequestMapping(params = {"partNumber"}, method = {org.springframework.web.bind.annotation.RequestMethod.DELETE})
    public org.springframework.http.ResponseEntity<java.lang.Object> deletePart(@org.jetbrains.annotations.NotNull()
    @org.springframework.web.bind.annotation.PathVariable(value = "bucketname")
    java.lang.String bucketName, @org.jetbrains.annotations.NotNull()
    @org.springframework.web.bind.annotation.PathVariable(value = "objectname")
    java.lang.String objectName, @org.springframework.web.bind.annotation.RequestParam(value = "partNumber")
    int partNumber) {
        return null;
    }
    
    public PartObjectController() {
        super();
    }
}