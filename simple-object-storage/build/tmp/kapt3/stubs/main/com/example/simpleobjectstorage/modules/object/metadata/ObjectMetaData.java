package com.example.simpleobjectstorage.modules.object.metadata;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 10}, bv = {1, 0, 2}, k = 1, d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0005\n\u0002\u0010\t\n\u0002\b\t\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0007\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002R\u001c\u0010\u0003\u001a\u0004\u0018\u00010\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0005\u0010\u0006\"\u0004\b\u0007\u0010\bR\"\u0010\t\u001a\u0004\u0018\u00010\n8\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0010\n\u0002\u0010\u000f\u001a\u0004\b\u000b\u0010\f\"\u0004\b\r\u0010\u000eR\u001c\u0010\u0010\u001a\u0004\u0018\u00010\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0011\u0010\u0006\"\u0004\b\u0012\u0010\bR \u0010\u0013\u001a\u0004\u0018\u00010\u00148\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0015\u0010\u0016\"\u0004\b\u0017\u0010\u0018\u00a8\u0006\u0019"}, d2 = {"Lcom/example/simpleobjectstorage/modules/object/metadata/ObjectMetaData;", "Lcom/example/simpleobjectstorage/utils/BaseEntity;", "()V", "body", "", "getBody", "()Ljava/lang/String;", "setBody", "(Ljava/lang/String;)V", "id", "", "getId", "()Ljava/lang/Long;", "setId", "(Ljava/lang/Long;)V", "Ljava/lang/Long;", "metadataKey", "getMetadataKey", "setMetadataKey", "objectStorage", "Lcom/example/simpleobjectstorage/modules/object/ObjectStorage;", "getObjectStorage", "()Lcom/example/simpleobjectstorage/modules/object/ObjectStorage;", "setObjectStorage", "(Lcom/example/simpleobjectstorage/modules/object/ObjectStorage;)V", "simple-object-storage"})
@javax.persistence.Entity()
public final class ObjectMetaData extends com.example.simpleobjectstorage.utils.BaseEntity {
    @org.jetbrains.annotations.Nullable()
    @org.hibernate.annotations.GenericGenerator(name = "native", strategy = "native")
    @javax.persistence.GeneratedValue(strategy = javax.persistence.GenerationType.AUTO, generator = "native")
    @javax.persistence.Id()
    private java.lang.Long id;
    @org.jetbrains.annotations.Nullable()
    private java.lang.String metadataKey;
    @org.jetbrains.annotations.Nullable()
    private java.lang.String body;
    @org.jetbrains.annotations.Nullable()
    @com.fasterxml.jackson.annotation.JsonBackReference()
    @javax.persistence.JoinColumn(name = "objectId")
    @javax.persistence.ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private com.example.simpleobjectstorage.modules.object.ObjectStorage objectStorage;
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Long getId() {
        return null;
    }
    
    public final void setId(@org.jetbrains.annotations.Nullable()
    java.lang.Long p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getMetadataKey() {
        return null;
    }
    
    public final void setMetadataKey(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getBody() {
        return null;
    }
    
    public final void setBody(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.example.simpleobjectstorage.modules.object.ObjectStorage getObjectStorage() {
        return null;
    }
    
    public final void setObjectStorage(@org.jetbrains.annotations.Nullable()
    com.example.simpleobjectstorage.modules.object.ObjectStorage p0) {
    }
    
    public ObjectMetaData() {
        super();
    }
}