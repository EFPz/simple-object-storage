package com.example.simpleobjectstorage.modules.object;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 10}, bv = {1, 0, 2}, k = 1, d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\t\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\bg\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001J\u0012\u0010\u0004\u001a\u0004\u0018\u00010\u00022\u0006\u0010\u0005\u001a\u00020\u0006H&J\u001a\u0010\u0007\u001a\u0004\u0018\u00010\u00022\u0006\u0010\b\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u0006H&\u00a8\u0006\u000b"}, d2 = {"Lcom/example/simpleobjectstorage/modules/object/ObjectStorageRepository;", "Lorg/springframework/data/jpa/repository/JpaRepository;", "Lcom/example/simpleobjectstorage/modules/object/ObjectStorage;", "", "findByName", "name", "", "findOneByBucketAndName", "bucket", "Lcom/example/simpleobjectstorage/modules/bucket/Bucket;", "ame", "simple-object-storage"})
@org.springframework.stereotype.Repository()
public abstract interface ObjectStorageRepository extends org.springframework.data.jpa.repository.JpaRepository<com.example.simpleobjectstorage.modules.object.ObjectStorage, java.lang.Long> {
    
    @org.jetbrains.annotations.Nullable()
    public abstract com.example.simpleobjectstorage.modules.object.ObjectStorage findByName(@org.jetbrains.annotations.NotNull()
    java.lang.String name);
    
    @org.jetbrains.annotations.Nullable()
    public abstract com.example.simpleobjectstorage.modules.object.ObjectStorage findOneByBucketAndName(@org.jetbrains.annotations.NotNull()
    com.example.simpleobjectstorage.modules.bucket.Bucket bucket, @org.jetbrains.annotations.NotNull()
    java.lang.String ame);
}