package com.example.simpleobjectstorage.modules.bucket.dto;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 10}, bv = {1, 0, 2}, k = 1, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002R \u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0006\u0010\u0007\"\u0004\b\b\u0010\t\u00a8\u0006\n"}, d2 = {"Lcom/example/simpleobjectstorage/modules/bucket/dto/BucketListDto;", "", "()V", "list", "", "Lorg/omg/CORBA/Object;", "getList", "()Ljava/util/List;", "setList", "(Ljava/util/List;)V", "simple-object-storage"})
public final class BucketListDto {
    @org.jetbrains.annotations.NotNull()
    private java.util.List<? extends org.omg.CORBA.Object> list;
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.List<org.omg.CORBA.Object> getList() {
        return null;
    }
    
    public final void setList(@org.jetbrains.annotations.NotNull()
    java.util.List<? extends org.omg.CORBA.Object> p0) {
    }
    
    public BucketListDto() {
        super();
    }
}