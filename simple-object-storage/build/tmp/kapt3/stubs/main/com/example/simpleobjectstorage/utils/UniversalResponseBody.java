package com.example.simpleobjectstorage.utils;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 10}, bv = {1, 0, 2}, k = 1, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010%\n\u0002\u0010\u000e\n\u0002\b\u0005\n\u0002\u0010!\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0010$\n\u0002\b\u0006\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u001e\u0010\u0011\u001a\u00020\u00002\u0006\u0010\u0012\u001a\u00020\u00052\u0006\u0010\u0013\u001a\u00020\u00052\u0006\u0010\u0014\u001a\u00020\u0005J\u0012\u0010\u0015\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00010\u0016J\u001a\u0010\u0017\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00010\u00042\u0006\u0010\u0018\u001a\u00020\fJ\u0018\u0010\u0019\u001a\u00020\u00002\u0006\u0010\u001a\u001a\u00020\u00052\b\u0010\u001b\u001a\u0004\u0018\u00010\u0001R&\u0010\u0003\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00010\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0006\u0010\u0007\"\u0004\b\b\u0010\tR \u0010\n\u001a\b\u0012\u0004\u0012\u00020\f0\u000bX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\r\u0010\u000e\"\u0004\b\u000f\u0010\u0010\u00a8\u0006\u001c"}, d2 = {"Lcom/example/simpleobjectstorage/utils/UniversalResponseBody;", "", "()V", "data", "", "", "getData", "()Ljava/util/Map;", "setData", "(Ljava/util/Map;)V", "fieldErrors", "", "Lorg/springframework/validation/FieldError;", "getFieldErrors", "()Ljava/util/List;", "setFieldErrors", "(Ljava/util/List;)V", "addFieldError", "field", "code", "message", "build", "", "mapFieldError", "fieldError", "put", "key", "value", "simple-object-storage"})
public final class UniversalResponseBody {
    @org.jetbrains.annotations.NotNull()
    private java.util.List<org.springframework.validation.FieldError> fieldErrors;
    @org.jetbrains.annotations.NotNull()
    private java.util.Map<java.lang.String, java.lang.Object> data;
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.List<org.springframework.validation.FieldError> getFieldErrors() {
        return null;
    }
    
    public final void setFieldErrors(@org.jetbrains.annotations.NotNull()
    java.util.List<org.springframework.validation.FieldError> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.Map<java.lang.String, java.lang.Object> getData() {
        return null;
    }
    
    public final void setData(@org.jetbrains.annotations.NotNull()
    java.util.Map<java.lang.String, java.lang.Object> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.example.simpleobjectstorage.utils.UniversalResponseBody addFieldError(@org.jetbrains.annotations.NotNull()
    java.lang.String field, @org.jetbrains.annotations.NotNull()
    java.lang.String code, @org.jetbrains.annotations.NotNull()
    java.lang.String message) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.example.simpleobjectstorage.utils.UniversalResponseBody put(@org.jetbrains.annotations.NotNull()
    java.lang.String key, @org.jetbrains.annotations.Nullable()
    java.lang.Object value) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.Map<java.lang.String, java.lang.Object> build() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.Map<java.lang.String, java.lang.Object> mapFieldError(@org.jetbrains.annotations.NotNull()
    org.springframework.validation.FieldError fieldError) {
        return null;
    }
    
    public UniversalResponseBody() {
        super();
    }
}