package com.example.simpleobjectstorage.modules.part;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 10}, bv = {1, 0, 2}, k = 1, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\t\n\u0000\n\u0002\u0010 \n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\bg\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001J\u0016\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u0007H&J\u001a\u0010\b\u001a\u0004\u0018\u00010\u00022\u0006\u0010\u0006\u001a\u00020\u00072\u0006\u0010\t\u001a\u00020\nH&\u00a8\u0006\u000b"}, d2 = {"Lcom/example/simpleobjectstorage/modules/part/PartObjectRepository;", "Lorg/springframework/data/jpa/repository/JpaRepository;", "Lcom/example/simpleobjectstorage/modules/part/PartObject;", "", "findAllByObjectStorageOrderByPartNumberAsc", "", "objectStorage", "Lcom/example/simpleobjectstorage/modules/object/ObjectStorage;", "findOneByObjectStorageAndPartNumberAndActiveIsTrue", "partNumber", "", "simple-object-storage"})
@org.springframework.stereotype.Repository()
public abstract interface PartObjectRepository extends org.springframework.data.jpa.repository.JpaRepository<com.example.simpleobjectstorage.modules.part.PartObject, java.lang.Long> {
    
    @org.jetbrains.annotations.Nullable()
    public abstract com.example.simpleobjectstorage.modules.part.PartObject findOneByObjectStorageAndPartNumberAndActiveIsTrue(@org.jetbrains.annotations.NotNull()
    com.example.simpleobjectstorage.modules.object.ObjectStorage objectStorage, int partNumber);
    
    @org.jetbrains.annotations.NotNull()
    public abstract java.util.List<com.example.simpleobjectstorage.modules.part.PartObject> findAllByObjectStorageOrderByPartNumberAsc(@org.jetbrains.annotations.NotNull()
    com.example.simpleobjectstorage.modules.object.ObjectStorage objectStorage);
}