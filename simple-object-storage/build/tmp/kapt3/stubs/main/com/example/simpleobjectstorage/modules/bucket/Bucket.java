package com.example.simpleobjectstorage.modules.bucket;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 10}, bv = {1, 0, 2}, k = 1, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0005\n\u0002\u0010!\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0007\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002R\u001e\u0010\u0003\u001a\u00020\u00048\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0005\u0010\u0006\"\u0004\b\u0007\u0010\bR\u001c\u0010\t\u001a\u0004\u0018\u00010\nX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u000b\u0010\f\"\u0004\b\r\u0010\u000eR$\u0010\u000f\u001a\b\u0012\u0004\u0012\u00020\u00110\u00108\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0012\u0010\u0013\"\u0004\b\u0014\u0010\u0015\u00a8\u0006\u0016"}, d2 = {"Lcom/example/simpleobjectstorage/modules/bucket/Bucket;", "Lcom/example/simpleobjectstorage/utils/BaseEntity;", "()V", "id", "", "getId", "()J", "setId", "(J)V", "name", "", "getName", "()Ljava/lang/String;", "setName", "(Ljava/lang/String;)V", "objectStorages", "", "Lcom/example/simpleobjectstorage/modules/object/ObjectStorage;", "getObjectStorages", "()Ljava/util/List;", "setObjectStorages", "(Ljava/util/List;)V", "simple-object-storage"})
@javax.persistence.Entity()
public final class Bucket extends com.example.simpleobjectstorage.utils.BaseEntity {
    @org.hibernate.annotations.GenericGenerator(name = "native", strategy = "native")
    @javax.persistence.GeneratedValue(strategy = javax.persistence.GenerationType.AUTO, generator = "native")
    @javax.persistence.Id()
    private long id;
    @org.jetbrains.annotations.Nullable()
    private java.lang.String name;
    @org.jetbrains.annotations.NotNull()
    @com.fasterxml.jackson.annotation.JsonManagedReference()
    @javax.persistence.OneToMany(mappedBy = "bucket", cascade = {javax.persistence.CascadeType.ALL})
    private java.util.List<com.example.simpleobjectstorage.modules.object.ObjectStorage> objectStorages;
    
    public final long getId() {
        return 0L;
    }
    
    public final void setId(long p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getName() {
        return null;
    }
    
    public final void setName(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.List<com.example.simpleobjectstorage.modules.object.ObjectStorage> getObjectStorages() {
        return null;
    }
    
    public final void setObjectStorages(@org.jetbrains.annotations.NotNull()
    java.util.List<com.example.simpleobjectstorage.modules.object.ObjectStorage> p0) {
    }
    
    public Bucket() {
        super();
    }
}