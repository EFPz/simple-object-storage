package com.example.simpleobjectstorage.modules.part.dto;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 10}, bv = {1, 0, 2}, k = 1, d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0005\n\u0002\u0010\b\n\u0002\b\u0006\n\u0002\u0010\u000e\n\u0002\b\u000b\n\u0002\u0010\t\n\u0002\b\t\u0018\u00002\u00020\u0001B\u000f\b\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004B\u0005\u00a2\u0006\u0002\u0010\u0005R\u001a\u0010\u0006\u001a\u00020\u0007X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\b\u0010\t\"\u0004\b\n\u0010\u000bR\u001e\u0010\f\u001a\u0004\u0018\u00010\rX\u0086\u000e\u00a2\u0006\u0010\n\u0002\u0010\u0012\u001a\u0004\b\u000e\u0010\u000f\"\u0004\b\u0010\u0010\u0011R\u001c\u0010\u0013\u001a\u0004\u0018\u00010\u0014X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0015\u0010\u0016\"\u0004\b\u0017\u0010\u0018R\u001c\u0010\u0019\u001a\u0004\u0018\u00010\u0014X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u001a\u0010\u0016\"\u0004\b\u001b\u0010\u0018R\u001c\u0010\u001c\u001a\u0004\u0018\u00010\u0014X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u001d\u0010\u0016\"\u0004\b\u001e\u0010\u0018R\u001e\u0010\u001f\u001a\u0004\u0018\u00010 X\u0086\u000e\u00a2\u0006\u0010\n\u0002\u0010%\u001a\u0004\b!\u0010\"\"\u0004\b#\u0010$R\u001e\u0010&\u001a\u0004\u0018\u00010\rX\u0086\u000e\u00a2\u0006\u0010\n\u0002\u0010\u0012\u001a\u0004\b\'\u0010\u000f\"\u0004\b(\u0010\u0011\u00a8\u0006)"}, d2 = {"Lcom/example/simpleobjectstorage/modules/part/dto/PartCreateDto;", "", "partObject", "Lcom/example/simpleobjectstorage/modules/part/PartObject;", "(Lcom/example/simpleobjectstorage/modules/part/PartObject;)V", "()V", "complete", "", "getComplete", "()Z", "setComplete", "(Z)V", "length", "", "getLength", "()Ljava/lang/Integer;", "setLength", "(Ljava/lang/Integer;)V", "Ljava/lang/Integer;", "location", "", "getLocation", "()Ljava/lang/String;", "setLocation", "(Ljava/lang/String;)V", "md5", "getMd5", "setMd5", "name", "getName", "setName", "obj", "", "getObj", "()Ljava/lang/Long;", "setObj", "(Ljava/lang/Long;)V", "Ljava/lang/Long;", "partNumber", "getPartNumber", "setPartNumber", "simple-object-storage"})
public final class PartCreateDto {
    @org.jetbrains.annotations.Nullable()
    private java.lang.String name;
    @org.jetbrains.annotations.Nullable()
    private java.lang.Integer partNumber;
    @org.jetbrains.annotations.Nullable()
    private java.lang.Integer length;
    private boolean complete;
    @org.jetbrains.annotations.Nullable()
    private java.lang.String location;
    @org.jetbrains.annotations.Nullable()
    private java.lang.Long obj;
    @org.jetbrains.annotations.Nullable()
    private java.lang.String md5;
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getName() {
        return null;
    }
    
    public final void setName(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Integer getPartNumber() {
        return null;
    }
    
    public final void setPartNumber(@org.jetbrains.annotations.Nullable()
    java.lang.Integer p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Integer getLength() {
        return null;
    }
    
    public final void setLength(@org.jetbrains.annotations.Nullable()
    java.lang.Integer p0) {
    }
    
    public final boolean getComplete() {
        return false;
    }
    
    public final void setComplete(boolean p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getLocation() {
        return null;
    }
    
    public final void setLocation(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Long getObj() {
        return null;
    }
    
    public final void setObj(@org.jetbrains.annotations.Nullable()
    java.lang.Long p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getMd5() {
        return null;
    }
    
    public final void setMd5(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
    
    public PartCreateDto() {
        super();
    }
    
    public PartCreateDto(@org.jetbrains.annotations.NotNull()
    com.example.simpleobjectstorage.modules.part.PartObject partObject) {
        super();
    }
}