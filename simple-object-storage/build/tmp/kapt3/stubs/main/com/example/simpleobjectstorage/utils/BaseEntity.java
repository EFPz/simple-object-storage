package com.example.simpleobjectstorage.utils;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 10}, bv = {1, 0, 2}, k = 1, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u000b\n\u0002\u0010\t\n\u0002\b\u0002\b\'\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002R\u001a\u0010\u0003\u001a\u00020\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0005\u0010\u0006\"\u0004\b\u0007\u0010\bR \u0010\t\u001a\u0004\u0018\u00010\n8\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u000b\u0010\f\"\u0004\b\r\u0010\u000eR \u0010\u000f\u001a\u0004\u0018\u00010\u00108\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0011\u0010\u0012\"\u0004\b\u0013\u0010\u0014R \u0010\u0015\u001a\u0004\u0018\u00010\n8\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0016\u0010\f\"\u0004\b\u0017\u0010\u000eR \u0010\u0018\u001a\u0004\u0018\u00010\u00108\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0019\u0010\u0012\"\u0004\b\u001a\u0010\u0014R\u0014\u0010\u001b\u001a\u0004\u0018\u00010\u001c8\u0002X\u0083\u0004\u00a2\u0006\u0004\n\u0002\u0010\u001d\u00a8\u0006\u001e"}, d2 = {"Lcom/example/simpleobjectstorage/utils/BaseEntity;", "Ljava/io/Serializable;", "()V", "active", "", "getActive", "()Z", "setActive", "(Z)V", "createdBy", "", "getCreatedBy", "()Ljava/lang/String;", "setCreatedBy", "(Ljava/lang/String;)V", "createdDate", "Ljava/util/Date;", "getCreatedDate", "()Ljava/util/Date;", "setCreatedDate", "(Ljava/util/Date;)V", "modifiedBy", "getModifiedBy", "setModifiedBy", "modifiedDate", "getModifiedDate", "setModifiedDate", "version", "", "Ljava/lang/Long;", "simple-object-storage"})
@javax.persistence.MappedSuperclass()
@javax.persistence.EntityListeners(value = {org.springframework.data.jpa.domain.support.AuditingEntityListener.class})
public abstract class BaseEntity implements java.io.Serializable {
    @org.jetbrains.annotations.Nullable()
    @javax.persistence.Column(updatable = false)
    @org.springframework.data.annotation.CreatedDate()
    @javax.persistence.Temporal(value = javax.persistence.TemporalType.TIMESTAMP)
    private java.util.Date createdDate;
    @org.jetbrains.annotations.Nullable()
    @javax.persistence.Column(updatable = false)
    @org.springframework.data.annotation.CreatedBy()
    private java.lang.String createdBy;
    @org.jetbrains.annotations.Nullable()
    @org.springframework.data.annotation.LastModifiedDate()
    @javax.persistence.Temporal(value = javax.persistence.TemporalType.TIMESTAMP)
    private java.util.Date modifiedDate;
    @org.jetbrains.annotations.Nullable()
    @org.springframework.data.annotation.LastModifiedBy()
    private java.lang.String modifiedBy;
    private boolean active;
    @javax.persistence.Column(name = "optlock", columnDefinition = "integer DEFAULT 0", nullable = false)
    @javax.persistence.Version()
    private final java.lang.Long version = null;
    
    @org.jetbrains.annotations.Nullable()
    public final java.util.Date getCreatedDate() {
        return null;
    }
    
    public final void setCreatedDate(@org.jetbrains.annotations.Nullable()
    java.util.Date p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getCreatedBy() {
        return null;
    }
    
    public final void setCreatedBy(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.util.Date getModifiedDate() {
        return null;
    }
    
    public final void setModifiedDate(@org.jetbrains.annotations.Nullable()
    java.util.Date p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getModifiedBy() {
        return null;
    }
    
    public final void setModifiedBy(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
    
    public final boolean getActive() {
        return false;
    }
    
    public final void setActive(boolean p0) {
    }
    
    public BaseEntity() {
        super();
    }
}