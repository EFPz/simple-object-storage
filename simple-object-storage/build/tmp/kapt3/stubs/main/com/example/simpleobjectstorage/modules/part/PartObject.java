package com.example.simpleobjectstorage.modules.part;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 10}, bv = {1, 0, 2}, k = 1, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\b\u0006\n\u0002\u0010\b\n\u0002\b\u0006\n\u0002\u0010\u000e\n\u0002\b\u000b\n\u0002\u0018\u0002\n\u0002\b\b\b\u0007\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002R\"\u0010\u0003\u001a\u0004\u0018\u00010\u00048\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0010\n\u0002\u0010\t\u001a\u0004\b\u0005\u0010\u0006\"\u0004\b\u0007\u0010\bR\u001e\u0010\n\u001a\u0004\u0018\u00010\u000bX\u0086\u000e\u00a2\u0006\u0010\n\u0002\u0010\u0010\u001a\u0004\b\f\u0010\r\"\u0004\b\u000e\u0010\u000fR\u001c\u0010\u0011\u001a\u0004\u0018\u00010\u0012X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0013\u0010\u0014\"\u0004\b\u0015\u0010\u0016R\u001c\u0010\u0017\u001a\u0004\u0018\u00010\u0012X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0018\u0010\u0014\"\u0004\b\u0019\u0010\u0016R\u001c\u0010\u001a\u001a\u0004\u0018\u00010\u0012X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u001b\u0010\u0014\"\u0004\b\u001c\u0010\u0016R \u0010\u001d\u001a\u0004\u0018\u00010\u001e8\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u001f\u0010 \"\u0004\b!\u0010\"R\u001e\u0010#\u001a\u0004\u0018\u00010\u000bX\u0086\u000e\u00a2\u0006\u0010\n\u0002\u0010\u0010\u001a\u0004\b$\u0010\r\"\u0004\b%\u0010\u000f\u00a8\u0006&"}, d2 = {"Lcom/example/simpleobjectstorage/modules/part/PartObject;", "Lcom/example/simpleobjectstorage/utils/BaseEntity;", "()V", "id", "", "getId", "()Ljava/lang/Long;", "setId", "(Ljava/lang/Long;)V", "Ljava/lang/Long;", "length", "", "getLength", "()Ljava/lang/Integer;", "setLength", "(Ljava/lang/Integer;)V", "Ljava/lang/Integer;", "location", "", "getLocation", "()Ljava/lang/String;", "setLocation", "(Ljava/lang/String;)V", "md5", "getMd5", "setMd5", "name", "getName", "setName", "objectStorage", "Lcom/example/simpleobjectstorage/modules/object/ObjectStorage;", "getObjectStorage", "()Lcom/example/simpleobjectstorage/modules/object/ObjectStorage;", "setObjectStorage", "(Lcom/example/simpleobjectstorage/modules/object/ObjectStorage;)V", "partNumber", "getPartNumber", "setPartNumber", "simple-object-storage"})
@javax.persistence.Entity()
public final class PartObject extends com.example.simpleobjectstorage.utils.BaseEntity {
    @org.jetbrains.annotations.Nullable()
    @org.hibernate.annotations.GenericGenerator(name = "native", strategy = "native")
    @javax.persistence.GeneratedValue(strategy = javax.persistence.GenerationType.AUTO, generator = "native")
    @javax.persistence.Id()
    private java.lang.Long id;
    @org.jetbrains.annotations.Nullable()
    private java.lang.String name;
    @org.jetbrains.annotations.Nullable()
    private java.lang.Integer partNumber;
    @org.jetbrains.annotations.Nullable()
    private java.lang.Integer length;
    @org.jetbrains.annotations.Nullable()
    private java.lang.String location;
    @org.jetbrains.annotations.Nullable()
    private java.lang.String md5;
    @org.jetbrains.annotations.Nullable()
    @com.fasterxml.jackson.annotation.JsonBackReference()
    @javax.persistence.JoinColumn(name = "objectStorageId")
    @javax.persistence.ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private com.example.simpleobjectstorage.modules.object.ObjectStorage objectStorage;
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Long getId() {
        return null;
    }
    
    public final void setId(@org.jetbrains.annotations.Nullable()
    java.lang.Long p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getName() {
        return null;
    }
    
    public final void setName(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Integer getPartNumber() {
        return null;
    }
    
    public final void setPartNumber(@org.jetbrains.annotations.Nullable()
    java.lang.Integer p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Integer getLength() {
        return null;
    }
    
    public final void setLength(@org.jetbrains.annotations.Nullable()
    java.lang.Integer p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getLocation() {
        return null;
    }
    
    public final void setLocation(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getMd5() {
        return null;
    }
    
    public final void setMd5(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.example.simpleobjectstorage.modules.object.ObjectStorage getObjectStorage() {
        return null;
    }
    
    public final void setObjectStorage(@org.jetbrains.annotations.Nullable()
    com.example.simpleobjectstorage.modules.object.ObjectStorage p0) {
    }
    
    public PartObject() {
        super();
    }
}