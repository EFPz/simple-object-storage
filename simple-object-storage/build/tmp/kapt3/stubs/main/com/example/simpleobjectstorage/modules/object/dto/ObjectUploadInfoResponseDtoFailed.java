package com.example.simpleobjectstorage.modules.object.dto;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 10}, bv = {1, 0, 2}, k = 1, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\b\n\u0002\b\u0012\u0018\u00002\u00020\u0001B\'\b\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0005\u0012\u0006\u0010\u0007\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\bB\u0005\u00a2\u0006\u0002\u0010\tR\u001c\u0010\u0007\u001a\u0004\u0018\u00010\u0003X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\n\u0010\u000b\"\u0004\b\f\u0010\rR\u001e\u0010\u0004\u001a\u0004\u0018\u00010\u0005X\u0086\u000e\u00a2\u0006\u0010\n\u0002\u0010\u0012\u001a\u0004\b\u000e\u0010\u000f\"\u0004\b\u0010\u0010\u0011R\u001c\u0010\u0002\u001a\u0004\u0018\u00010\u0003X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0013\u0010\u000b\"\u0004\b\u0014\u0010\rR\u001e\u0010\u0006\u001a\u0004\u0018\u00010\u0005X\u0086\u000e\u00a2\u0006\u0010\n\u0002\u0010\u0012\u001a\u0004\b\u0015\u0010\u000f\"\u0004\b\u0016\u0010\u0011\u00a8\u0006\u0017"}, d2 = {"Lcom/example/simpleobjectstorage/modules/object/dto/ObjectUploadInfoResponseDtoFailed;", "", "md5", "", "length", "", "partNumber", "error", "(Ljava/lang/String;IILjava/lang/String;)V", "()V", "getError", "()Ljava/lang/String;", "setError", "(Ljava/lang/String;)V", "getLength", "()Ljava/lang/Integer;", "setLength", "(Ljava/lang/Integer;)V", "Ljava/lang/Integer;", "getMd5", "setMd5", "getPartNumber", "setPartNumber", "simple-object-storage"})
public final class ObjectUploadInfoResponseDtoFailed {
    @org.jetbrains.annotations.Nullable()
    private java.lang.String md5;
    @org.jetbrains.annotations.Nullable()
    private java.lang.Integer length;
    @org.jetbrains.annotations.Nullable()
    private java.lang.Integer partNumber;
    @org.jetbrains.annotations.Nullable()
    private java.lang.String error;
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getMd5() {
        return null;
    }
    
    public final void setMd5(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Integer getLength() {
        return null;
    }
    
    public final void setLength(@org.jetbrains.annotations.Nullable()
    java.lang.Integer p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Integer getPartNumber() {
        return null;
    }
    
    public final void setPartNumber(@org.jetbrains.annotations.Nullable()
    java.lang.Integer p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getError() {
        return null;
    }
    
    public final void setError(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
    
    public ObjectUploadInfoResponseDtoFailed() {
        super();
    }
    
    public ObjectUploadInfoResponseDtoFailed(@org.jetbrains.annotations.NotNull()
    java.lang.String md5, int length, int partNumber, @org.jetbrains.annotations.NotNull()
    java.lang.String error) {
        super();
    }
}