package com.example.simpleobjectstorage.controllers;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 10}, bv = {1, 0, 2}, k = 1, d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0004\b\u0017\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0018\u0010\u000f\u001a\b\u0012\u0004\u0012\u00020\u00010\u00102\b\b\u0001\u0010\u0011\u001a\u00020\u0012H\u0017J\u0018\u0010\u0013\u001a\b\u0012\u0004\u0012\u00020\u00010\u00102\b\b\u0001\u0010\u0011\u001a\u00020\u0012H\u0017J\u0018\u0010\u0014\u001a\b\u0012\u0004\u0012\u00020\u00010\u00102\b\b\u0001\u0010\u0011\u001a\u00020\u0012H\u0017J\b\u0010\u0015\u001a\u00020\u0012H\u0017R\u001e\u0010\u0003\u001a\u00020\u00048\u0016@\u0016X\u0097.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0005\u0010\u0006\"\u0004\b\u0007\u0010\bR\u001e\u0010\t\u001a\u00020\n8\u0016@\u0016X\u0097.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u000b\u0010\f\"\u0004\b\r\u0010\u000e\u00a8\u0006\u0016"}, d2 = {"Lcom/example/simpleobjectstorage/controllers/BucketController;", "", "()V", "bucketRepository", "Lcom/example/simpleobjectstorage/modules/bucket/BucketRepository;", "getBucketRepository", "()Lcom/example/simpleobjectstorage/modules/bucket/BucketRepository;", "setBucketRepository", "(Lcom/example/simpleobjectstorage/modules/bucket/BucketRepository;)V", "bucketService", "Lcom/example/simpleobjectstorage/modules/bucket/BucketService;", "getBucketService", "()Lcom/example/simpleobjectstorage/modules/bucket/BucketService;", "setBucketService", "(Lcom/example/simpleobjectstorage/modules/bucket/BucketService;)V", "createBucket", "Lorg/springframework/http/ResponseEntity;", "name", "", "deleteBucket", "listBucket", "status", "simple-object-storage"})
@org.springframework.web.bind.annotation.CrossOrigin()
@org.springframework.web.bind.annotation.RequestMapping(value = {"{bucketname}"})
@org.springframework.web.bind.annotation.RestController()
public class BucketController {
    @org.jetbrains.annotations.NotNull()
    @org.springframework.beans.factory.annotation.Autowired()
    public com.example.simpleobjectstorage.modules.bucket.BucketService bucketService;
    @org.jetbrains.annotations.NotNull()
    @org.springframework.beans.factory.annotation.Autowired()
    public com.example.simpleobjectstorage.modules.bucket.BucketRepository bucketRepository;
    
    @org.jetbrains.annotations.NotNull()
    public com.example.simpleobjectstorage.modules.bucket.BucketService getBucketService() {
        return null;
    }
    
    public void setBucketService(@org.jetbrains.annotations.NotNull()
    com.example.simpleobjectstorage.modules.bucket.BucketService p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public com.example.simpleobjectstorage.modules.bucket.BucketRepository getBucketRepository() {
        return null;
    }
    
    public void setBucketRepository(@org.jetbrains.annotations.NotNull()
    com.example.simpleobjectstorage.modules.bucket.BucketRepository p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    @org.springframework.web.bind.annotation.RequestMapping(params = {"create"}, method = {org.springframework.web.bind.annotation.RequestMethod.POST})
    public org.springframework.http.ResponseEntity<java.lang.Object> createBucket(@org.jetbrains.annotations.NotNull()
    @org.springframework.web.bind.annotation.PathVariable(value = "bucketname")
    java.lang.String name) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @org.springframework.web.bind.annotation.RequestMapping(params = {"delete"}, method = {org.springframework.web.bind.annotation.RequestMethod.DELETE})
    public org.springframework.http.ResponseEntity<java.lang.Object> deleteBucket(@org.jetbrains.annotations.NotNull()
    @org.springframework.web.bind.annotation.PathVariable(value = "bucketname")
    java.lang.String name) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @org.springframework.web.bind.annotation.RequestMapping(params = {"list"}, method = {org.springframework.web.bind.annotation.RequestMethod.GET})
    public org.springframework.http.ResponseEntity<java.lang.Object> listBucket(@org.jetbrains.annotations.NotNull()
    @org.springframework.web.bind.annotation.PathVariable(value = "bucketname")
    java.lang.String name) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @org.springframework.web.bind.annotation.RequestMapping(method = {org.springframework.web.bind.annotation.RequestMethod.GET}, value = {"/status", "/status/"})
    public java.lang.String status() {
        return null;
    }
    
    public BucketController() {
        super();
    }
}