package com.example.simpleobjectstorage.modules.object.dto;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 10}, bv = {1, 0, 2}, k = 1, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\b\n\u0002\b\u000f\u0018\u00002\u00020\u0001B\u001f\b\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0007B\u0005\u00a2\u0006\u0002\u0010\bR\u001e\u0010\u0004\u001a\u0004\u0018\u00010\u0005X\u0086\u000e\u00a2\u0006\u0010\n\u0002\u0010\r\u001a\u0004\b\t\u0010\n\"\u0004\b\u000b\u0010\fR\u001c\u0010\u0002\u001a\u0004\u0018\u00010\u0003X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u000e\u0010\u000f\"\u0004\b\u0010\u0010\u0011R\u001e\u0010\u0006\u001a\u0004\u0018\u00010\u0005X\u0086\u000e\u00a2\u0006\u0010\n\u0002\u0010\r\u001a\u0004\b\u0012\u0010\n\"\u0004\b\u0013\u0010\f\u00a8\u0006\u0014"}, d2 = {"Lcom/example/simpleobjectstorage/modules/object/dto/ObjectUploadInfoResponseDtoSuccess;", "", "md5", "", "length", "", "partNumber", "(Ljava/lang/String;II)V", "()V", "getLength", "()Ljava/lang/Integer;", "setLength", "(Ljava/lang/Integer;)V", "Ljava/lang/Integer;", "getMd5", "()Ljava/lang/String;", "setMd5", "(Ljava/lang/String;)V", "getPartNumber", "setPartNumber", "simple-object-storage"})
public final class ObjectUploadInfoResponseDtoSuccess {
    @org.jetbrains.annotations.Nullable()
    private java.lang.String md5;
    @org.jetbrains.annotations.Nullable()
    private java.lang.Integer length;
    @org.jetbrains.annotations.Nullable()
    private java.lang.Integer partNumber;
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getMd5() {
        return null;
    }
    
    public final void setMd5(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Integer getLength() {
        return null;
    }
    
    public final void setLength(@org.jetbrains.annotations.Nullable()
    java.lang.Integer p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Integer getPartNumber() {
        return null;
    }
    
    public final void setPartNumber(@org.jetbrains.annotations.Nullable()
    java.lang.Integer p0) {
    }
    
    public ObjectUploadInfoResponseDtoSuccess() {
        super();
    }
    
    public ObjectUploadInfoResponseDtoSuccess(@org.jetbrains.annotations.NotNull()
    java.lang.String md5, int length, int partNumber) {
        super();
    }
}