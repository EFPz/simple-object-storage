package com.example.simpleobjectstorage.modules.object.metadata;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 10}, bv = {1, 0, 2}, k = 1, d1 = {"\u0000B\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0004\b\u0017\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J \u0010\u001b\u001a\u00020\u001c2\u0006\u0010\u001d\u001a\u00020\u001e2\u0006\u0010\u001f\u001a\u00020\u001e2\u0006\u0010 \u001a\u00020\u001eH\u0017J\u0018\u0010!\u001a\u00020\"2\u0006\u0010\u001d\u001a\u00020\u001e2\u0006\u0010\u001f\u001a\u00020\u001eH\u0017J \u0010#\u001a\u00020\"2\u0006\u0010\u001d\u001a\u00020\u001e2\u0006\u0010\u001f\u001a\u00020\u001e2\u0006\u0010 \u001a\u00020\u001eH\u0017J(\u0010$\u001a\u00020\u001c2\u0006\u0010\u001d\u001a\u00020\u001e2\u0006\u0010\u001f\u001a\u00020\u001e2\u0006\u0010 \u001a\u00020\u001e2\u0006\u0010%\u001a\u00020\u001eH\u0017R\u001e\u0010\u0003\u001a\u00020\u00048\u0016@\u0016X\u0097.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0005\u0010\u0006\"\u0004\b\u0007\u0010\bR\u001e\u0010\t\u001a\u00020\n8\u0016@\u0016X\u0097.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u000b\u0010\f\"\u0004\b\r\u0010\u000eR\u001e\u0010\u000f\u001a\u00020\u00108\u0016@\u0016X\u0097.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0011\u0010\u0012\"\u0004\b\u0013\u0010\u0014R\u001e\u0010\u0015\u001a\u00020\u00168\u0016@\u0016X\u0097.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0017\u0010\u0018\"\u0004\b\u0019\u0010\u001a\u00a8\u0006&"}, d2 = {"Lcom/example/simpleobjectstorage/modules/object/metadata/ObjectMetaDataService;", "", "()V", "bucketRepository", "Lcom/example/simpleobjectstorage/modules/bucket/BucketRepository;", "getBucketRepository", "()Lcom/example/simpleobjectstorage/modules/bucket/BucketRepository;", "setBucketRepository", "(Lcom/example/simpleobjectstorage/modules/bucket/BucketRepository;)V", "objectMetaDataRepository", "Lcom/example/simpleobjectstorage/modules/object/metadata/ObjectMetaDataRepository;", "getObjectMetaDataRepository", "()Lcom/example/simpleobjectstorage/modules/object/metadata/ObjectMetaDataRepository;", "setObjectMetaDataRepository", "(Lcom/example/simpleobjectstorage/modules/object/metadata/ObjectMetaDataRepository;)V", "objectStorageRepository", "Lcom/example/simpleobjectstorage/modules/object/ObjectStorageRepository;", "getObjectStorageRepository", "()Lcom/example/simpleobjectstorage/modules/object/ObjectStorageRepository;", "setObjectStorageRepository", "(Lcom/example/simpleobjectstorage/modules/object/ObjectStorageRepository;)V", "partObjectRepository", "Lcom/example/simpleobjectstorage/modules/part/PartObjectRepository;", "getPartObjectRepository", "()Lcom/example/simpleobjectstorage/modules/part/PartObjectRepository;", "setPartObjectRepository", "(Lcom/example/simpleobjectstorage/modules/part/PartObjectRepository;)V", "deleteMetadata", "", "bucketName", "", "objectName", "key", "getAllMetadata", "Lcom/example/simpleobjectstorage/utils/UniversalResponseBody;", "getMetadataByKey", "updateMetadata", "body", "simple-object-storage"})
@org.springframework.stereotype.Service()
public class ObjectMetaDataService {
    @org.jetbrains.annotations.NotNull()
    @org.springframework.beans.factory.annotation.Autowired()
    public com.example.simpleobjectstorage.modules.object.ObjectStorageRepository objectStorageRepository;
    @org.jetbrains.annotations.NotNull()
    @org.springframework.beans.factory.annotation.Autowired()
    public com.example.simpleobjectstorage.modules.bucket.BucketRepository bucketRepository;
    @org.jetbrains.annotations.NotNull()
    @org.springframework.beans.factory.annotation.Autowired()
    public com.example.simpleobjectstorage.modules.part.PartObjectRepository partObjectRepository;
    @org.jetbrains.annotations.NotNull()
    @org.springframework.beans.factory.annotation.Autowired()
    public com.example.simpleobjectstorage.modules.object.metadata.ObjectMetaDataRepository objectMetaDataRepository;
    
    @org.jetbrains.annotations.NotNull()
    public com.example.simpleobjectstorage.modules.object.ObjectStorageRepository getObjectStorageRepository() {
        return null;
    }
    
    public void setObjectStorageRepository(@org.jetbrains.annotations.NotNull()
    com.example.simpleobjectstorage.modules.object.ObjectStorageRepository p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public com.example.simpleobjectstorage.modules.bucket.BucketRepository getBucketRepository() {
        return null;
    }
    
    public void setBucketRepository(@org.jetbrains.annotations.NotNull()
    com.example.simpleobjectstorage.modules.bucket.BucketRepository p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public com.example.simpleobjectstorage.modules.part.PartObjectRepository getPartObjectRepository() {
        return null;
    }
    
    public void setPartObjectRepository(@org.jetbrains.annotations.NotNull()
    com.example.simpleobjectstorage.modules.part.PartObjectRepository p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public com.example.simpleobjectstorage.modules.object.metadata.ObjectMetaDataRepository getObjectMetaDataRepository() {
        return null;
    }
    
    public void setObjectMetaDataRepository(@org.jetbrains.annotations.NotNull()
    com.example.simpleobjectstorage.modules.object.metadata.ObjectMetaDataRepository p0) {
    }
    
    @org.springframework.transaction.annotation.Transactional()
    public void updateMetadata(@org.jetbrains.annotations.NotNull()
    java.lang.String bucketName, @org.jetbrains.annotations.NotNull()
    java.lang.String objectName, @org.jetbrains.annotations.NotNull()
    java.lang.String key, @org.jetbrains.annotations.NotNull()
    java.lang.String body) {
    }
    
    @org.springframework.transaction.annotation.Transactional()
    public void deleteMetadata(@org.jetbrains.annotations.NotNull()
    java.lang.String bucketName, @org.jetbrains.annotations.NotNull()
    java.lang.String objectName, @org.jetbrains.annotations.NotNull()
    java.lang.String key) {
    }
    
    @org.jetbrains.annotations.NotNull()
    @org.springframework.transaction.annotation.Transactional()
    public com.example.simpleobjectstorage.utils.UniversalResponseBody getMetadataByKey(@org.jetbrains.annotations.NotNull()
    java.lang.String bucketName, @org.jetbrains.annotations.NotNull()
    java.lang.String objectName, @org.jetbrains.annotations.NotNull()
    java.lang.String key) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @org.springframework.transaction.annotation.Transactional()
    public com.example.simpleobjectstorage.utils.UniversalResponseBody getAllMetadata(@org.jetbrains.annotations.NotNull()
    java.lang.String bucketName, @org.jetbrains.annotations.NotNull()
    java.lang.String objectName) {
        return null;
    }
    
    public ObjectMetaDataService() {
        super();
    }
}