package com.example.simpleobjectstorage.modules.object;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 10}, bv = {1, 0, 2}, k = 1, d1 = {"\u0000l\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0012\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\b\u0017\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0010\u0010\u0015\u001a\u00020\u00162\u0006\u0010\u0017\u001a\u00020\u0016H\u0017J\u0018\u0010\u0018\u001a\u00020\u00192\u0006\u0010\u001a\u001a\u00020\u001b2\u0006\u0010\u001c\u001a\u00020\u001bH\u0017J \u0010\u001d\u001a\u00020\u00192\u0006\u0010\u001e\u001a\u00020\u001f2\u0006\u0010 \u001a\u00020\u001b2\u0006\u0010!\u001a\u00020\u001bH\u0017J \u0010\"\u001a\u00020\u00192\u0006\u0010\u001a\u001a\u00020\u001b2\u0006\u0010\u001c\u001a\u00020\u001b2\u0006\u0010#\u001a\u00020\u001fH\u0017J \u0010$\u001a\u00020%2\u0016\u0010&\u001a\u0012\u0012\u0004\u0012\u00020(0\'j\b\u0012\u0004\u0012\u00020(`)H\u0017J\u0018\u0010*\u001a\u00020\u00162\u0006\u0010\u001a\u001a\u00020\u001b2\u0006\u0010\u001c\u001a\u00020\u001bH\u0017J\u0018\u0010+\u001a\u00020\u00162\u0006\u0010\u001a\u001a\u00020\u001b2\u0006\u0010\u001c\u001a\u00020\u001bH\u0017J\u0010\u0010,\u001a\u00020\u00192\u0006\u0010-\u001a\u00020\u001bH\u0017J\u0018\u0010.\u001a\u00020\u00192\u0006\u0010\u001a\u001a\u00020\u001b2\u0006\u0010\u001c\u001a\u00020\u001bH\u0017J8\u0010/\u001a\u00020\u00192\u0006\u0010\u001a\u001a\u00020\u001b2\u0006\u0010\u001c\u001a\u00020\u001b2\u0006\u00100\u001a\u00020\u001f2\u0006\u00101\u001a\u00020\u001f2\u0006\u00102\u001a\u0002032\u0006\u00104\u001a\u000205H\u0017J\u001a\u00106\u001a\u0004\u0018\u00010\u00162\u0006\u0010\u001a\u001a\u00020\u001b2\u0006\u0010\u001c\u001a\u00020\u001bH\u0017J8\u00107\u001a\u00020\u001b2\u0006\u0010\u001a\u001a\u00020\u001b2\u0006\u0010\u001c\u001a\u00020\u001b2\u0006\u0010#\u001a\u00020\u001f2\u0006\u0010\u001e\u001a\u00020\u001f2\u0006\u0010 \u001a\u00020\u001b2\u0006\u00108\u001a\u000209H\u0017R\u001e\u0010\u0003\u001a\u00020\u00048\u0016@\u0016X\u0097.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0005\u0010\u0006\"\u0004\b\u0007\u0010\bR\u001e\u0010\t\u001a\u00020\n8\u0016@\u0016X\u0097.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u000b\u0010\f\"\u0004\b\r\u0010\u000eR\u001e\u0010\u000f\u001a\u00020\u00108\u0016@\u0016X\u0097.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0011\u0010\u0012\"\u0004\b\u0013\u0010\u0014\u00a8\u0006:"}, d2 = {"Lcom/example/simpleobjectstorage/modules/object/ObjectStorageService;", "", "()V", "bucketRepository", "Lcom/example/simpleobjectstorage/modules/bucket/BucketRepository;", "getBucketRepository", "()Lcom/example/simpleobjectstorage/modules/bucket/BucketRepository;", "setBucketRepository", "(Lcom/example/simpleobjectstorage/modules/bucket/BucketRepository;)V", "objectStorageRepository", "Lcom/example/simpleobjectstorage/modules/object/ObjectStorageRepository;", "getObjectStorageRepository", "()Lcom/example/simpleobjectstorage/modules/object/ObjectStorageRepository;", "setObjectStorageRepository", "(Lcom/example/simpleobjectstorage/modules/object/ObjectStorageRepository;)V", "partObjectRepository", "Lcom/example/simpleobjectstorage/modules/part/PartObjectRepository;", "getPartObjectRepository", "()Lcom/example/simpleobjectstorage/modules/part/PartObjectRepository;", "setPartObjectRepository", "(Lcom/example/simpleobjectstorage/modules/part/PartObjectRepository;)V", "calculateETagAndMD5", "Lcom/example/simpleobjectstorage/modules/object/ObjectStorage;", "objectStorage", "checkCompleteStatus", "", "bucketName", "", "objectName", "checkHeaderValidity", "length", "", "md5", "location", "checkTicketValidity", "partNumber", "combineBytes", "Ljava/io/InputStream;", "allBytes", "Ljava/util/ArrayList;", "", "Lkotlin/collections/ArrayList;", "complete", "create", "deleteFile", "path", "deleteObject", "download", "from", "to", "response", "Ljavax/servlet/http/HttpServletResponse;", "getAllParts", "", "getByName", "uploadPart", "request", "Ljavax/servlet/http/HttpServletRequest;", "simple-object-storage"})
@org.springframework.stereotype.Service()
public class ObjectStorageService {
    @org.jetbrains.annotations.NotNull()
    @org.springframework.beans.factory.annotation.Autowired()
    public com.example.simpleobjectstorage.modules.object.ObjectStorageRepository objectStorageRepository;
    @org.jetbrains.annotations.NotNull()
    @org.springframework.beans.factory.annotation.Autowired()
    public com.example.simpleobjectstorage.modules.bucket.BucketRepository bucketRepository;
    @org.jetbrains.annotations.NotNull()
    @org.springframework.beans.factory.annotation.Autowired()
    public com.example.simpleobjectstorage.modules.part.PartObjectRepository partObjectRepository;
    
    @org.jetbrains.annotations.NotNull()
    public com.example.simpleobjectstorage.modules.object.ObjectStorageRepository getObjectStorageRepository() {
        return null;
    }
    
    public void setObjectStorageRepository(@org.jetbrains.annotations.NotNull()
    com.example.simpleobjectstorage.modules.object.ObjectStorageRepository p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public com.example.simpleobjectstorage.modules.bucket.BucketRepository getBucketRepository() {
        return null;
    }
    
    public void setBucketRepository(@org.jetbrains.annotations.NotNull()
    com.example.simpleobjectstorage.modules.bucket.BucketRepository p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public com.example.simpleobjectstorage.modules.part.PartObjectRepository getPartObjectRepository() {
        return null;
    }
    
    public void setPartObjectRepository(@org.jetbrains.annotations.NotNull()
    com.example.simpleobjectstorage.modules.part.PartObjectRepository p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    @org.springframework.transaction.annotation.Transactional()
    public com.example.simpleobjectstorage.modules.object.ObjectStorage create(@org.jetbrains.annotations.NotNull()
    java.lang.String bucketName, @org.jetbrains.annotations.NotNull()
    java.lang.String objectName) {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    @org.springframework.transaction.annotation.Transactional()
    public com.example.simpleobjectstorage.modules.object.ObjectStorage getByName(@org.jetbrains.annotations.NotNull()
    java.lang.String bucketName, @org.jetbrains.annotations.NotNull()
    java.lang.String objectName) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @org.springframework.transaction.annotation.Transactional()
    public java.lang.String uploadPart(@org.jetbrains.annotations.NotNull()
    java.lang.String bucketName, @org.jetbrains.annotations.NotNull()
    java.lang.String objectName, int partNumber, int length, @org.jetbrains.annotations.NotNull()
    java.lang.String md5, @org.jetbrains.annotations.NotNull()
    javax.servlet.http.HttpServletRequest request) {
        return null;
    }
    
    @org.springframework.transaction.annotation.Transactional()
    public void deleteFile(@org.jetbrains.annotations.NotNull()
    java.lang.String path) {
    }
    
    @org.springframework.transaction.annotation.Transactional()
    public void checkTicketValidity(@org.jetbrains.annotations.NotNull()
    java.lang.String bucketName, @org.jetbrains.annotations.NotNull()
    java.lang.String objectName, int partNumber) {
    }
    
    @org.springframework.transaction.annotation.Transactional()
    public void checkCompleteStatus(@org.jetbrains.annotations.NotNull()
    java.lang.String bucketName, @org.jetbrains.annotations.NotNull()
    java.lang.String objectName) {
    }
    
    @org.springframework.transaction.annotation.Transactional()
    public void checkHeaderValidity(int length, @org.jetbrains.annotations.NotNull()
    java.lang.String md5, @org.jetbrains.annotations.NotNull()
    java.lang.String location) {
    }
    
    @org.jetbrains.annotations.NotNull()
    @org.springframework.transaction.annotation.Transactional()
    public com.example.simpleobjectstorage.modules.object.ObjectStorage complete(@org.jetbrains.annotations.NotNull()
    java.lang.String bucketName, @org.jetbrains.annotations.NotNull()
    java.lang.String objectName) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @org.springframework.transaction.annotation.Transactional()
    public com.example.simpleobjectstorage.modules.object.ObjectStorage calculateETagAndMD5(@org.jetbrains.annotations.NotNull()
    com.example.simpleobjectstorage.modules.object.ObjectStorage objectStorage) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @org.springframework.transaction.annotation.Transactional()
    public java.io.InputStream combineBytes(@org.jetbrains.annotations.NotNull()
    java.util.ArrayList<byte[]> allBytes) {
        return null;
    }
    
    @org.springframework.transaction.annotation.Transactional()
    public void deleteObject(@org.jetbrains.annotations.NotNull()
    java.lang.String bucketName, @org.jetbrains.annotations.NotNull()
    java.lang.String objectName) {
    }
    
    @org.springframework.transaction.annotation.Transactional()
    public void download(@org.jetbrains.annotations.NotNull()
    java.lang.String bucketName, @org.jetbrains.annotations.NotNull()
    java.lang.String objectName, int from, int to, @org.jetbrains.annotations.NotNull()
    javax.servlet.http.HttpServletResponse response, boolean getAllParts) {
    }
    
    public ObjectStorageService() {
        super();
    }
}