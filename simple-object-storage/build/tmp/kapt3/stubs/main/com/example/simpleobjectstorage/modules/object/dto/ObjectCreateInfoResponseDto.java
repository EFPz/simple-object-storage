package com.example.simpleobjectstorage.modules.object.dto;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 10}, bv = {1, 0, 2}, k = 1, d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\t\n\u0002\b\t\n\u0002\u0010\u000e\n\u0002\b\u0005\u0018\u00002\u00020\u0001B\u000f\b\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004B\u0005\u00a2\u0006\u0002\u0010\u0005R\u001e\u0010\u0006\u001a\u0004\u0018\u00010\u0007X\u0086\u000e\u00a2\u0006\u0010\n\u0002\u0010\f\u001a\u0004\b\b\u0010\t\"\u0004\b\n\u0010\u000bR\u001e\u0010\r\u001a\u0004\u0018\u00010\u0007X\u0086\u000e\u00a2\u0006\u0010\n\u0002\u0010\f\u001a\u0004\b\u000e\u0010\t\"\u0004\b\u000f\u0010\u000bR\u001c\u0010\u0010\u001a\u0004\u0018\u00010\u0011X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0012\u0010\u0013\"\u0004\b\u0014\u0010\u0015\u00a8\u0006\u0016"}, d2 = {"Lcom/example/simpleobjectstorage/modules/object/dto/ObjectCreateInfoResponseDto;", "", "obj", "Lcom/example/simpleobjectstorage/modules/object/ObjectStorage;", "(Lcom/example/simpleobjectstorage/modules/object/ObjectStorage;)V", "()V", "create", "", "getCreate", "()Ljava/lang/Long;", "setCreate", "(Ljava/lang/Long;)V", "Ljava/lang/Long;", "modified", "getModified", "setModified", "name", "", "getName", "()Ljava/lang/String;", "setName", "(Ljava/lang/String;)V", "simple-object-storage"})
public final class ObjectCreateInfoResponseDto {
    @org.jetbrains.annotations.Nullable()
    private java.lang.Long create;
    @org.jetbrains.annotations.Nullable()
    private java.lang.Long modified;
    @org.jetbrains.annotations.Nullable()
    private java.lang.String name;
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Long getCreate() {
        return null;
    }
    
    public final void setCreate(@org.jetbrains.annotations.Nullable()
    java.lang.Long p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Long getModified() {
        return null;
    }
    
    public final void setModified(@org.jetbrains.annotations.Nullable()
    java.lang.Long p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getName() {
        return null;
    }
    
    public final void setName(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
    
    public ObjectCreateInfoResponseDto() {
        super();
    }
    
    public ObjectCreateInfoResponseDto(@org.jetbrains.annotations.NotNull()
    com.example.simpleobjectstorage.modules.object.ObjectStorage obj) {
        super();
    }
}