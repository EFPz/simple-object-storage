#!/usr/bin/env python3
import os
import logging
import json
import uuid
import redis
import requests
import sys
import hashlib
from io import BytesIO
# sys.path.append('../Thumbnailer')
# import make_thumbnail


LOG = logging
REDIS_QUEUE_LOCATION = os.getenv('REDIS_QUEUE', 'localhost')
QUEUE_NAME = 'queue:thumbnail'

INSTANCE_NAME = uuid.uuid4().hex

LOG.basicConfig(
    level=LOG.DEBUG,
    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s'
)

def watch_queue(redis_conn, queue_name, callback_func, timeout=30):
    active = True

    while active:
        # Fetch a json-encoded task using a blocking (left) pop
        packed = redis_conn.blpop([queue_name], timeout=timeout)

        if not packed:
            # if nothing is returned, poll a again
            continue

        _, packed_task = packed

        # If it's treated to a poison pill, quit the loop
        if packed_task == b'DIE':
            active = False
        else:
            task = None
            try:
                task = json.loads(packed_task)
            except Exception:
                LOG.exception('json.loads failed')
            if task:
                callback_func(task)

def upload_gif(bucket_name,gif_path_raw):
    gif_path = gif_path_raw[2:]
    # Delete object for gif_path first, ignore even if it doesn't exist
    requests.delete("http://localhost:8080/{}/{}?delete".format(bucket_name,gif_path))
    # Create object
    requests.post("http://localhost:8080/{}/{}?create".format(bucket_name,gif_path))
    # Upload gif_path
    url = "http://localhost:8080/{}/{}?partNumber=1".format(bucket_name,gif_path)
    # Get length
    file = open(gif_path,"rb")
    length = str(file.seek(0,2))
    file.close()

    # Get md5
    hash_md5 = hashlib.md5()
    with open(gif_path, "rb") as f:
        for chunk in iter(lambda: f.read(4096), b""):
            hash_md5.update(chunk)
    md5 = hash_md5.hexdigest()

    # print(length)
    # print(md5)
    headers = {"Content-Length": length,"Content-MD5": md5}
    requests.put(url,headers = headers ,data = open(gif_path,"rb"))
    # Mark as Complete
    requests.post("http://localhost:8080/{}/{}?complete".format(bucket_name,gif_path))


def make_thumbnail(log,task):
    bucket_name = task.get('bucket')
    object_name = task.get('object')
    obj_name_without_ext , _ = os.path.splitext(object_name)
    # print(bucket_name,object_name)
    url = "http://localhost:8080/{}/{}".format(bucket_name,object_name)
    headers = {"Range":"bytes=0-"}
    video = requests.get(url, headers = headers)
    # print(video.content)
    # print(obj_name_without_ext)
    # save the video to the current path
    print(type(video.content))
    video_path = "./{}".format(object_name)
    with open(video_path,"wb") as f:
        f.write(video.content)
        f.close()

    result_path = "./{}.gif".format(obj_name_without_ext)
    print(video_path,result_path)
    os.system("./make_thumbnail {} {}".format(video_path,result_path))    # Generate gif
    upload_gif(bucket_name,result_path)

    # Delete the video after making gif
    os.remove(video_path)
    # Delete gif file
    os.remove(result_path)



def main():
    LOG.info('Starting a worker...')
    LOG.info('Unique name: %s', INSTANCE_NAME)
    host, *port_info = REDIS_QUEUE_LOCATION.split(':')
    port = tuple()
    if port_info:
        port, *_ = port_info
        port = (int(port),)

    named_logging = LOG.getLogger(name=INSTANCE_NAME)
    named_logging.info('Trying to connect to %s [%s]', host, REDIS_QUEUE_LOCATION)
    redis_conn = redis.Redis(host=host, *port)
    watch_queue(
        redis_conn,
        QUEUE_NAME,
        lambda task_descr: make_thumbnail(named_logging, task_descr))


if __name__ == '__main__':
    main()
