import Vue from "vue";
import Router from "vue-router";
import Home from "./views/Home.vue";
import DisplayRoom from "./views/DisplayRoom.vue";
import ControlCenter from "./views/ControlCenter.vue";

Vue.use(Router);

export default new Router({
  mode: "history",
  base: process.env.BASE_URL,
  routes: [
    // {
    //   path: "/:bucketName",
    //   name: "home",
    //   props: true,
    //   component: Home
    // },
    {
      path: "/",
      name: "home",
      component: Home
    },
    {
      path: "/about",
      name: "about",
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () =>
        import(/* webpackChunkName: "about" */ "./views/About.vue")
    },
    // {
    //   path: "/display",
    //   name: "display",
    //   component: DisplayRoom,
    //   props: true
    // },
    // {
    //   path: "/control",
    //   name: "control",
    //   component: ControlCenter
    // },
    {
      path: "/display/:bucketName",
      name: "display",
      component: DisplayRoom,
      props: true
    },
    {
      path: "/control/:bucketName",
      name: "control",
      component: ControlCenter,
      props: true
    },
  ]
});
