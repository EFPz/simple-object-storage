import os
import json
import redis
from flask import Flask, jsonify, request
import requests


app = Flask(__name__)

class RedisResource:
    REDIS_QUEUE_LOCATION = os.getenv('REDIS_QUEUE', 'localhost')
    QUEUE_NAME = 'queue:thumbnail'

    host, *port_info = REDIS_QUEUE_LOCATION.split(':')
    port = tuple()
    if port_info:
        port, *_ = port_info
        port = (int(port),)

    conn = redis.Redis(host=host, *port)

def check_bucket_validity(bucket_name):
    # print("http://localhost:8080/%s"% bucket_name)
    # %s is %d years old." % (name, age)
    result = requests.get("http://localhost:8080/%s?list"% bucket_name)
    return result

def check_object_validity(bucket_name,object_name):
    result = requests.get("http://localhost:8080/%s/%s?list"%(bucket_name,object_name))
    return result

# def post_thumb_job(bucket_name, object_name):
#     body = request.json
#     json_packed = json.dumps(body)
#     print('packed:', json_packed)
#     RedisResource.conn.rpush(
#         RedisResource.QUEUE_NAME,
#         json_packed)
#
#     return jsonify({'status': 'OK'})


@app.route('/thumbnail/<bucket_name>', methods=['POST'])
def post_thumb_job(bucket_name):
    bucket = check_bucket_validity(bucket_name)
    if not bucket.status_code == 200:
        return jsonify({'error': "Bucket: {} doesn't exist.".format(bucket_name) }), 400
    objects = bucket.json()["objects"]
    for obj in objects:
        object_name = obj["name"]
        ext = object_name.split(".")[-1]
        print(object_name)
        print(ext)
        if ext not in ["mp4", "avi", "mov"]:
            print("ignore")
            continue
        if not check_object_validity(bucket_name,object_name).status_code == requests.codes.ok:
            print("Object not exist")
            return jsonify({'error': "Object: {} doesn't exist.".format(object_name) }), 400
        # Send to queue
        json_packed = json.dumps({"bucket": bucket_name , "object": object_name})
        print(json_packed)
        print('packed:', json_packed)
        RedisResource.conn.rpush(
            RedisResource.QUEUE_NAME,
            json_packed)
    return jsonify({"status":"jobs created"}), 200

@app.route('/thumbnail/<bucket_name>/<object_name>', methods=['POST'])
def post_thumb_job_single(bucket_name,object_name):
    bucket = check_bucket_validity(bucket_name)
    if not bucket.status_code == 200:
        return jsonify({'error': "Bucket: {} doesn't exist.".format(bucket_name) }), 400
    obj = check_object_validity(bucket_name,object_name)
    if not obj.status_code == requests.codes.ok:
        print("Object not exist")
        return jsonify({'error': "Object: {} doesn't exist.".format(object_name) }), 400

    object_name_no_ext = obj.json()["Object"]["name"]
    ext = object_name_no_ext.split(".")[-1]
    # print(object_name)
    # print(ext)
    if ext not in ["mp4", "avi", "mov"]:
        print("ignore")
        return jsonify({'error': "Object: {} doesn't have the right extension.".format(object_name) }), 400
    json_packed = json.dumps({"bucket": bucket_name , "object": object_name})
    print(json_packed)
    print('packed:', json_packed)
    RedisResource.conn.rpush(
        RedisResource.QUEUE_NAME,
        json_packed)
    return jsonify({"status":"Single jobs created"}), 200
    # print(objects)
    # return objects

    # return jsonify({"List": bucket.objects}), 200

    # for obj in objects:
    #     print(obj.name)

    # json_packed = json.dumps(body)
    # print('packed:', json_packed)
    # RedisResource.conn.rpush(
    #     RedisResource.QUEUE_NAME,
    #     json_packed)

    # return jsonify({'status': 'OK'})
